<?php
	/* Template Name: Page - Home */

	// Reset Session Variables
  session_start();
  session_unset();
  session_destroy();

	get_header();

	// CAROUSEL
	include('module/carousel/carousel.php');

	// CONTENT
	

?>

		<?php
			// CTA Buttons
			include('module/cta-buttons.php');
		?>

		<div class="content">
			<div class="container">
				<div class="contentwrap">
					<div class="row">

						<div class="col-sm-8">
							<div class="main">
								<!-- Loop -->
								<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
									<?php the_content(); ?>
								<?php endwhile; ?>
								<!-- END Loop -->
							</div>
						</div>
						<div class="col-sm-4">
							<div class="aside">
								<?php //get_sidebar(); ?>
							</div>
						</div>

						

					</div>
				</div>
			</div>
		</div>
	</div>
<?php get_footer(); ?>
