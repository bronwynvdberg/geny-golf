<?php

 // include_once('module/!theme-options.php');
  include_once('module/class/sbm-object.php');
  include_once('module/class/carousel.php');
  include_once('module/class/slide.php');
  include_once('module/class/content.php');


  $buster = time();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  
  <?php
    //  META VALUES FOR POST/PAGE - META FIELD GROUP 
    global $post;
    $metaTitle = "";
    if( is_home() ){
      $blogID = get_site_option( 'page_for_posts' );
      if( !get_field( 'seo_title', $blogID ) ){
        $metaTitle = get_the_title( $blogID );
      } else{
        $metaTitle = get_field( 'seo_title', $blogID );
      }
    } else {
      if( is_archive() ){
        $archive = get_post_type_labels( get_post_type_object( get_post_type($post->ID) ) );
        $metaTitle = $archive->name;
      } else{
        if( !get_field( 'seo_title', $post->ID ) ){
          $metaTitle = get_the_title( $post->ID );
        } else{
          $metaTitle = get_field( 'seo_title', $post->ID );
        }
      }
    } 
    $metaTitle .= " | ".get_bloginfo( 'name' );

  ?>
    <title><?php echo $metaTitle; ?></title>

    <!-- Meta -->
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="description" content="">
    <meta name="keywords" content="HTML,CSS,XML,JavaScript">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
 


    <link rel="profile" href="http://gmpg.org/xfn/11" />

    <!-- Icons -->
    <link rel="icon" href="<?php bloginfo( 'template_url' ); ?>/assets/icons/favicon.png">
    <link rel="apple-touch-startup-image" href="<?php bloginfo( 'template_url' ); ?>/assets/icons/favicon.png">
    <link rel="apple-touch-icon-precomposed" href="<?php bloginfo( 'template_url' ); ?>/assets/icons/apple-icon-57.png" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php bloginfo( 'template_url' ); ?>/assets/icons/apple-icon-72.png" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php bloginfo( 'template_url' ); ?>/assets/icons/apple-icon-114.png" />

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,800" rel="stylesheet">

    <!-- CSS -->
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>?version=<?=$buster?>" />
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/assets/css/swipebox.min.css">
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/assets/css/jasny-bootstrap.min.css">

    <!-- jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/assets/js/jquery.swipebox.min.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/assets/js/full_width.js?version=<?=$buster?>"></script>

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <script src="<?php bloginfo( 'template_url' ); ?>/assets/js/jquery.placeholder.js"></script>
    <![endif]-->

    <script>
      // IE @media fix
      if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
        var msViewportStyle = document.createElement("style")
        msViewportStyle.appendChild(
          document.createTextNode(
            "@-ms-viewport{width:auto!important}"
          )
        )
        document.getElementsByTagName("head")[0].appendChild(msViewportStyle)
      }

      // Compress Header
      <?php
        $scroll_top = 0;
        if (is_front_page()) {
          $scroll_top = 50;
        }
      ?>
      var scroll_top = <?php echo $scroll_top; ?>;
      var bannerHeight = 1000;
      var menuHeight = 100;

      $(document).scroll(function() {
        // Scroll to bottom of banner
        // menuHeight = $('.header').height() + 1; // +1 forces the heading transition
        // if ($(document).scrollTop() > bannerHeight - menuHeight) { }
        if ($(document).scrollTop() >= scroll_top) {
          $('.desktop-nav').addClass('alt-header');
        } else {
          $('.desktop-nav').removeClass('alt-header');
        }
      });

    </script>

    <!-- Wordpress Header -->
    <?php wp_head(); ?>

</head>

<body <?php body_class($class); ?>>
  <!--[if lt IE 7]>
    <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
  <![endif]-->
  <a href="#content" class="sr-only">Skip to main content</a>
  <div class="cover" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse"></div>
  <header>
    <?php
    //include("module/header/header-hamburger.php");    //Menu hidden, slide out from right on cilck > hamburger icon
    include("module/header/header-full-width.php");   //menu below logo, in new row. spans 12 col
    ?>
  </header>

  <div class="wrap">
