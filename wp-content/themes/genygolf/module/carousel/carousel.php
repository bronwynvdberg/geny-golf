<?php
	/*
			CAROUSEL
		
	*/
	$carousel = Carousel::FromOptions();


	if( have_rows( 'slides','options' ) ){
		while( have_rows( 'slides', 'options' ) ){
			the_row();
			$new_slide = Slide::FromOptions();
			$carousel->slide[] = $new_slide;

		}
	}
	

 
	// If Slider is empty but 'use_placeholders' and 'placeholder' is not, then use these.
	if (is_null($carousel->slide) && $carousel->use_placeholders && count($carousel->placeholders) > 0) {
		$slide_count = 0;
		foreach ($carousel->placeholders as $slide) {
			$new_slide = new Slide();
			$new_slide->title = "Carousel slide ".(++$slide_count);
			$new_slide->content = "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>";
			$new_slide->image = $slide;
			$carousel->slide[] = $new_slide;
		}
	}

	// Build slider
	if (count($carousel->slide) > 0) {
		$slide_count = 0;
		$indicators = "";
		$inner = "";
		$preload = "";          // Background images don't pre-load, these will.
		foreach ($carousel->slide as $slide) {
			$active = '';
			if ($slide_count == 0) {
				$active = 'active';
			}
			$preload .= "<img src=\"{$slide->image['url']}\" alt=\"\">";
			$indicators .= "\t\t\t<li data-target=\"#carousel_home\" data-slide-to=\"{$slide_count}\" class=\"{$active}\"></li>\r\n";
			$inner .= "
				<div class=\"carousel-item {$active}\">
					<div class=\"img\" style=\"height:{$carousel->height}px; background-image:url('{$slide->image['url']}');\"></div>
			";
			if ($carousel->captions) {
				$inner .= "
					<div class=\"carousel-caption\">
				    <h3 class=\"animated bounceInRight\" style=\"animation-delay:0.5s\">".embolden($slide->title)."</h3>
				    <div class=\"animated bounceInRight\" style=\"animation-delay:1s\">
				    	{$slide->content}
				    </div>
				  </div>
				";
			}
			$inner .= "
				</div>
			";
			$slide_count++;
		}
?>
<h2 class="sr-only">Homepage Carousel</h2>
<div id="carousel_home" class="carousel <?=$carousel->animation?>" data-ride="carousel" style="background-color:<?=$carousel->bgcolour?>">
	<div class="pre-load"><?=$preload?></div>
	<?php
		// Indicators..
		if (count($carousel->slide) > 1) {
			echo "
		<ul class=\"carousel-indicators indicator-{$carousel->indicator}\">
			{$indicators}
		</ul>
			";
		}
		// Slides
	?>
	<div class="carousel-inner" data-height="<?=$carousel->height?>" style="height:<?=$carousel->height?>px;">
		<?=$inner?>
	</div>
	<?php
		// Left / Right Arrows
		if (count($carousel->slide) > 1 && $carousel->navigation) {
	?>
	<a class="carousel-control-prev" href="#carousel_home" data-slide="prev">
		<span class="carousel-control-prev-icon"></span>
	</a>
	<a class="carousel-control-next" href="#carousel_home" data-slide="next">
		<span class="carousel-control-next-icon"></span>
	</a>
	<?php
			}
		}
	?>
</div>

<script>
	// CAROUSEL
	$(function() {
		if ($('#carousel_home')) {
			$('#carousel_home').carousel({
				interval: <?=$carousel->delay?>
			});
		}
	});
</script>

