	<section class="<?=$module->id?> cta <?=$module->width?> <?=$module->position?> <?=$module->background?>" style="<?=$module->style?>">
		<div class="container block-bg">
			<div class="row">
				<div class="col-md-6 cta-block">
					<?=$module->title?>
					<?php
						if (!empty($module->content)) {
							echo $module->content;
						}
					?>
				</div>
				<div class="col-md-6 cta-button">
					<div>
						<?php
							if (!empty($module->button_text)) {
								echo "<a href=\"{$module->button_link}\" class=\"{$module->button_style}\">{$module->button_text}</a>";
							}
						?>
					</div>
				</div>
			</div>
		</div>
	</section>
