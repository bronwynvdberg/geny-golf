	<section class="<?=$module->id?> image-panel <?=$module->width?> <?=$module->position?> <?=$module->background?>" style="background-image:url(<?=$module->image->url?>);<?=$module->style?>">
		<div class="container">
			<div class="row">
				<div class="col-md-6 offset-col-6"></div>
				<div class="col-md-6 over-image"></div>
				<div class="col-md-6 cta-block block-bg">
					<div>
						<?=$module->title?>
						<?php
							if (!empty($module->content)) {
								echo $module->content;
							}
						?>
					</div>
				</div>
			</div>
		</div>
	</section>
