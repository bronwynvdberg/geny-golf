	<?php
		$head_office = null;
		foreach ($module->offices as $office) {
			if ($office->primary) {
				$head_office = [
					'address' => $office->address['address'],
					'phone'   => $office->phone,
					'email'   => $office->email,
				];
				// Break Address into lines - keeping the Level and street numbers together
				$head_office['address'] = explode(", ", $head_office['address']);
				if (strpos(strtolower($head_office['address'][0]), 'level') > -1) {
					$head_office['address'][0] = array_shift($head_office['address']).', '.$head_office['address'][0];
					$head_office['address'] = implode(",<br>", $head_office['address']);
				}
			}
		}
	?>
	<section class="<?=$module->id?> contact-equal-split <?=$module->width?> <?=$module->position?> <?=$module->background?>" style="<?=$module->style?>">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<div <?php
						if (!is_null($module->font) && !empty($module->font)) {
							echo "style=\"color:{$module->font};\"";
						}
						?>>
						<?=$module->title?>
						<?php
							if (!empty($module->content)) {
								$content = str_replace("##address##", $head_office['address'], $module->content);
								$content = str_replace("##phone##", $head_office['phone'], $content);
								$content = str_replace("##email##", $head_office['email'], $content);
								echo $content;
							}
						?>
					</div>
					<?php include('social.php'); ?>
				</div>
				<div class="col-md-6">
					<?php echo do_shortcode('[gravityform id="1" title="false" description="false" ajax="false"]'); ?>
				</div>
			</div>
		</div>
	</section>


