<?php
	// Alternate Panels
	//
	// $feature_item
	$order1 = "order-".($feature_item%2+1);
	$order2 = "order-".(2-$feature_item%2);
	$style = ($feature_item%2==0)?"grad_secondary_rl":"grad_primary_lr";
	return "
		<div class=\"feature-card feature-ap {$style} col-12\">
			<a href=\"##button_link##\" class=\"row block-bg\">
				<div class=\"col-md-4 {$order1} feature-image\" style=\"background-image:url('##background-image##')\"></div>
				<div class=\"col-md-8 {$order2}\">
					##title##
					<div>
						##content##
						<button class=\"##button_style##\">##button_text##</button>
					</div>
				</div>
			</a>
		</div>
	";
?>