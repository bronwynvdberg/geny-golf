<?php
	// Title, Content & Button
	//
	return "
		<div class=\"feature-card feature-tcb##style##\">
			<a href=\"##button_link##\">
				##title##
				<div>
					##content##
					<button class=\"##button_style##\">##button_text##</button>
				</div>
			</a>
		</div>
	";
?>