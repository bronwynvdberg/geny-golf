<?php
	// Post Background
	//
	return "
		<div class=\"feature-card feature-pb##style##\">
			<a href=\"##button_link##\" style=\"background-image:url('##background-image##')\">
				##title##
				<div>
					##excerpt##
					<button class=\"##button_style##\">##button_text##</button>
				</div>
			</a>
		</div>
	";
?>