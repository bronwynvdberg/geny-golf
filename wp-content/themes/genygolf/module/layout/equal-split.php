	<section class="<?=$module->id?> equal-split <?=$module->width?> <?=$module->position?> <?=$module->background?>" style="<?=$module->style?>">
		<div class="container">
			<div class="row">
				<div class="col-md-6 offset-col-6"></div>
				<div class="col-md-6 over-image" style="background-image:url(<?=$module->image->url?>);"></div>
				<div class="col-md-6 cta-block block-bg">
					<div>
						<div <?php
							if (!is_null($module->font) && !empty($module->font)) {
								echo "style=\"color:{$module->font};\"";
							}
							?>>
							<?=$module->title?>
							<?php
								if (!empty($module->content)) {
									echo $module->content;
								}
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
