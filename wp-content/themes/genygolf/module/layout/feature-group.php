	<section class="<?=$module->id?> feature-group <?=$module->width?> <?=$module->position?> <?=$module->background?>" style="<?=$module->style?>">
		<div class="container">
			<div class="row">

				<?php
					$feature_item = 0;
					if (!is_null($module->features) && !empty($module->features)) {
						foreach ($module->features as $feature) {
							$feature_card = include("features/{$module->feature_style}.php");
							$feature_card = str_replace("##style##", " col-sm-4", $feature_card);
							$feature_card = str_replace("##title##", $feature->title, $feature_card);
							$feature_card = str_replace("##content##", $feature->content, $feature_card);
							$feature_card = str_replace("##excerpt##", $feature->excerpt, $feature_card);
							$feature_card = str_replace("##button_text##", $feature->button_text, $feature_card);
							$feature_card = str_replace("##button_style##", $feature->button_style, $feature_card);
							$feature_card = str_replace("##background-image##", $feature->image->url, $feature_card);
							if (is_null($feature->button_link) || empty($feature->button_link) || $feature->button_link=="#") {
								$feature->button_link = $feature->permalink;
							}
							$feature_card = str_replace("##button_link##", $feature->button_link, $feature_card);

							echo $feature_card;
							$feature_item++;
						}
					}

				?>


			</div>
		</div>
	</section>
