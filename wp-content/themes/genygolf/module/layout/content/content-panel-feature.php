	<section class="<?=$module->id?> content-panel <?=$module->width?> <?=$module->position?> <?=$module->background?>" style="<?=$module->style?>">
		<div class="container">
			<div class="row">
				<div class="col-12 cta-block">
					<div>
						<?php
							if (!is_null($module->image->url) && !empty($module->image->url)) {
								echo "<img class=\"content-image\" src=\"{$module->image->url}\" alt=\"{$module->image->alt}\" align=\"right\">";
							}

							echo $module->title;

							if (!is_null($module->intro) && !empty($module->intro)) {
								echo "<div class=\"content-panel-intro\">{$module->intro}</div>";
							}

							if (!empty($module->content)) {
								echo $module->content;
							}
						?>
					</div>
				</div>
			</div>
		</div>
	</section>
