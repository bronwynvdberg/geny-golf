	<section class="<?=$module->id?> content-panel <?=$module->width?> <?=$module->position?> <?=$module->background?>" style="<?=$module->style?>">
		<div class="container">
			<div class="row">
				<div class="col-lg-7">
					<?=$module->title?>
				</div>
				<div class="col-lg-5 news-meta">
					<div><?=get_the_date()?></div>
				</div>
				<div class="col-12 cta-block">
					<?php

						if (!is_null($module->image->url) && !empty($module->image->url)) {
							echo "<img class=\"content-image\" src=\"{$module->image->url}\" alt=\"{$module->image->alt}\" align=\"right\">";
						}

						if (!is_null($module->intro) && !empty($module->intro)) {
							echo "<div class=\"content-panel-intro\">{$module->intro}</div>";
						}

						if (!empty($module->content)) {
							echo $module->content;
						}
					?>
				</div>
			</div>
		</div>
	</section>
