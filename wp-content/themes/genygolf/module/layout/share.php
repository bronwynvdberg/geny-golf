	<section class="<?=$module->id?> share-links <?=$module->width?> <?=$module->position?> <?=$module->background?>" style="<?=$module->style?>">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<?php
						if ($module->title_tag != "module_title") {
							echo "
								{$module->title}
								<ul>
							";
						} else {
							echo "
								<ul>
									<li>{$module->title}</li>
							";
						}
						if (!is_null($module->links) && !empty($module->links)) {
							foreach ($module->links as $link) {
								echo "<li><a class=\"btn btn-share btn-{$link->share}\" href=\"{$link->link}\" target=\"_blank\"><i class=\"fa fa-{$link->share}\" aria-hidden=\"true\"></i></a></li>";
							}
						}
					?>
					</ul>
				</div>
			</div>
		</div>
	</section>
