<?php
	// LATEST PROJECTS
	//
	$projects = [];
	$project_bg = validateImage(get_field('project_bg','options'),['w'=>1600,'h'=>600]);

	$page_id = get_the_id();

  $loop = new WP_Query(
    array(
      'post_type' => 'project',
      'posts_per_page' => 4,
    )
  );
  while ( $loop->have_posts() ) {
    $loop->the_post();
    $new_project = Project::FromPost();
    // Don't add if is current project
    if ($page_id != $new_project->id) {
	    $projects[] = $new_project;
	  }
  }
  wp_reset_query();
?>
	<div class="content feature-projects" style="background-image:url('<?=$project_bg['url']?>');">
		<div class="container">
			<h2><strong>Latest</strong> Projects</h2>
			<div class="row">
				<?php
					foreach ($projects as $project) {
						echo "
							<div class=\"col-lg-4\">
								<a class=\"project\" href=\"{$project->permalink}\">
									<div class=\"img\" style=\"background-image:url('{$project->feature['url']}');\"></div>
									<h3>".embolden($project->title)."</h3>
									<p>{$project->excerpt}</p>
								</a>
							</div>
						";
					}
				?>
			</div>
		</div>
	</div>