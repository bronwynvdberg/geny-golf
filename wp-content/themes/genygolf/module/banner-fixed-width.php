		<!-- BANNER -->
		<div class="banner">
			<?php if(has_post_thumbnail()) :?>
			    <?php the_post_thumbnail('full', array('class' => 'img-responsive')); ?>
			<?php else : ?>
			    <img src="<?php bloginfo('template_url'); ?>/assets/images/banners/banner-content.png" class="img-responsive">
			<?php endif; ?>

			<div class="container">
				<div class="contentwrap">
					<div class="main col-sm-12">
						<h1><?php the_title(); ?></h1>
						<?php
							$header_byline = get_field('heading_byline');
							 if(!is_null($header_byline)) {
							 	echo '<p>'.$header_byline.'</p>';
							}
						?>
					</div>
				</div>
			</div>
		</div>
