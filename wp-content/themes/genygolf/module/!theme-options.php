<?php
	/*
			GLOBAL SETTINGS

	*/
	// Contact Page
	$contact = get_page_by_path('contact-us');
	$contact_id = $contact->ID;
	$home = get_page_by_path('home');
	$home_id = $home->ID;
	// Contact Info
	$GLOBALS['contacts']   = [
		'phone'     => get_field('phone', $contact_id),
		'address'   => get_field('address', $contact_id),
		'email'     => get_field('email', $contact_id),
		  'subject' => get_field('email_subject', $contact_id),
		  'body'    => get_field('email_body', $contact_id),
		'sydney'    => get_field('address_sydney', $contact_id),
	];
	// Social
	$GLOBALS['socials']   = [
		'facebook' => get_field('facebook', $contact_id),
		'instagram'=> get_field('instagram', $contact_id),
		'youtube'  => get_field('youtube', $contact_id),
		'twitter'  => get_field('twitter', $contact_id),
		'snapchat' => get_field('snapchat', $contact_id),
		'linkedin' => get_field('linked_in', $contact_id),
	]; 
	// Map
	$GLOBALS['location']   = get_field('location', $contact_id); 

	// Output Globals
	//
	function globals_output() {
		echo "::Global Data::<br>";

		echo "<br>: Contacts<br>";
		foreach ($GLOBALS['contacts'] as $key => $value) {
			if (!is_null($value) && !empty($value)) {
				echo "{$key}: '{$value}'<br>";
			}
		}

		echo "<br>: Socials<br>";
		foreach ($GLOBALS['socials'] as $key => $value) {
			if (!is_null($value) && !empty($value)) {
				echo "{$key}: '{$value}'<br>";
			}
		}

		echo "<br>: Map Location<br>";
		echo "location: '{$GLOBALS['location']}'<br>";
	}

?>