<?php
	// Breadcrumbs
	//
?>
					<div class="col-12 breadcrumbs">
						<?php
							if (isset($_SESSION['category'])) {
								if ($_SESSION['category']['title'] != $page_title) {
									echo "<a href=\"{$_SESSION['category']['url']}\">{$_SESSION['category']['title']}</a> | ";
								}
							}
							if (isset($_SESSION['service'])) {
								if ($_SESSION['service']['title'] != $page_title) {
									echo "<a href=\"{$_SESSION['service']['url']}\">{$_SESSION['service']['title']}</a> | ";
								}
							}
							echo $page_title;
						?>
					</div>
<?php

// print_r($_SESSION);

?>