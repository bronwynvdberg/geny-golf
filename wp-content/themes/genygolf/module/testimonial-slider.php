<?php
$query = new WP_Query(array(
    'post_type' => 'testimonials'
));
if( $query->have_posts() ){
?>
<div id="carousel-testimonial" class="carousel testimonial" data-ride="carousel" data-interval="12000" data-pause="hover" >
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <?php $loop = new WP_Query( array( 'post_type' => 'testimonials') ); ?>
      <?php $a=0; while ( $loop->have_posts() ) : $loop->the_post(); ?>

      <li data-target="#carousel-testimonial" data-slide-to="<?php echo $a; ?>" class="<?php if($a==0){ echo 'active';}else{ echo ''; } ?>"></li>

    <?php $a++; endwhile; wp_reset_query(); ?>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <?php $loop = new WP_Query( array( 'post_type' => 'testimonials') ); ?>
      <?php $b=0; while ( $loop->have_posts() ) : $loop->the_post(); ?>
        <div class="item <?php if($b==0){ echo 'active';}else{ echo ''; } ?>">
              <div class="carousel-caption">

                <h5><?php the_field('testimonial_long'); ?></h5>
              <span class="author" itemprop="author" itemscope itemtype="http://schema.org/Person"><span itemprop="name"><?php the_field('testimonial_person'); ?></span>, <span><?php the_field('testimonial_location'); ?></span>
              </div>

        </div>
    <?php $b++; endwhile; wp_reset_query(); ?>
  </div>


</div>

<?php } else { ?>
  <div class="text-center">No Testimonial, please add your first testimonial.</div>
<?php }
?>
