<?php
	// Modal
?>
	<div class="modal fade" id="getQuote">
	  <div class="modal-dialog modal-lg modal-dialog-centered">
	    <div class="modal-content">
	      <div class="modal-body">
  	      <button type="button" class="close" data-dismiss="modal">&times;</button>
  	      <?php echo do_shortcode('[gravityform id="2" title="true" description="true" ajax="false"]'); ?>
	      </div>
	    </div>
	  </div>
	</div>
