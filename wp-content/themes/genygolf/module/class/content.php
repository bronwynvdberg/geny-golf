<?php
	class Content {

		public $id;
		public $title;
		public $slug;
		public $copy;
		public $tag;
		public $image;
		public $permalink;

		public function __construct() {
			$this->id         = null;
			$this->title      = null;
			$this->slug       = null;
			$this->copy       = null;
			$this->tag        = null;
			$this->image      = null;
			$this->permalink  = null;
		}

		// FROM POST
		// ------------------------------------------------------------------------
		// #pre  = Naming definition to be added before field name
		// #size = An associative array with width/height defined : ['w'=>200,'h'=>40]
		//
		public static function FromPost($pre="",$size=null) {
			if (!empty($pre)) {
				$pre .= "_"; 
			}
			$instance            = new self();
			$instance->id        = get_the_id();
			$instance->title     = get_field($pre.'page_title');
			if (is_null($instance->title) || empty($instance->title)) {
				$instance->title   = get_the_title($instance->id);
			}
			$instance->copy      = get_field($pre.'body_content');
			if (is_null($instance->copy) || empty($instance->copy)) {
				$copy   		= get_post($instance->id);
				$instance->copy   = $copy->post_content;
			}
			$instance->image     = get_the_post_thumbnail_url();
			if (!isset($instance->image['url'])) {
				$instance->image   = validateImage([
					'url' => $instance->image,
					'alt' => '',
				],$size);
			} else {
				$instance->image   = validateImage($instance->image, $size);
			}
			$instance->permalink = get_the_permalink();

			return $instance;
		}

	
		public static function FromCategory($category) {
			$instance           = new self();
			$instance->title    = get_the_title();
			if (is_null($instance->title) || empty($instance->title)) {
				$cat = get_category_by_slug($category);
				$instance->title = $cat->name;
			}
			$instance->slug     = $category;
			$instance->tag      = get_field($category.'_tag','options');
			$instance->image    = validateImage(get_field($category.'_image','options'));
			return $instance;
		}

	}

?>

