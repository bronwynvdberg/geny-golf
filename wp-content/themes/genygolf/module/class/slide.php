<?php
	class Slide {
		
		public $id;
		public $title;
		public $content;
		public $image;
		public $alignment;

		public function __construct() {
			$this->id        = null;
			$this->title     = null;
			$this->content   = null;
			$this->image     = null;
			$this->alignment = "center";
		}

		public static function FromOptions() {
			$image = get_sub_field('slide_image');
			$instance            = new self();
			$instance->title     = get_sub_field('slide_heading');
			$instance->content   = get_sub_field('slide_content');
			$instance->image     = validateImage([
				'url' => $image['url'],
				'alt' => '',
			],[
				'w'=>2000,
				'h'=>1000
			]);
			$align               = get_sub_field('image_alignment');
			$instance->alignment = !empty($align)?$align:"center";
			return $instance;
		}

	}
?>