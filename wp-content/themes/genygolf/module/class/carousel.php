<?php
	class Carousel {

		public $id;
		public $height;
		public $animation;
		public $indicator;
		public $navigation;
		public $bgcolour;
		public $captions;
		public $delay;
		public $use_placeholders;
		public $placeholders;
		public $slides;

		public function __construct() {
			$this->id               = null;
		  	$this->height           = null;
			$this->animation        = null;
			$this->indicator        = null;
			$this->navigation       = null;
			$this->bgcolour         = null;
			$this->captions         = 0;
			$this->delay            = 1;
			$this->use_placeholders = null;
			$this->placeholders     = null;
			$this->slides           = [];
		}

		public static function FromOptions() {
			$instance                   = new self();
			$instance->height           = get_field('carousel_height','options');
			$instance->animation        = get_field('animation_style','options');
			$instance->indicator        = get_field('indicator_style','options');
			$instance->navigation       = get_field('navigation_arrows','options');
			$instance->bgcolour         = get_field('carousel_background_colour','options');
			$instance->captions         = get_field('show_captions','options');
			$instance->delay            = get_field('animation_delay','options') * 1000;
			$instance->use_placeholders = is_array(get_field('use_placeholders','options'));
			$placeholders               = get_field('placeholders','options');
			foreach ($placeholders as $placeholder) {
				$instance->placeholders[] = $placeholder['image_urls'];
			}
			return $instance;
		}

	}
?>