<?php
/* SOCIAL
*/
	$socials = get_field('socials','options');
	echo "<div class=\"social\">";
	foreach ($socials as $social) {
		echo "<a href=\"{$social['url']}\" target=\"_blank\" class=\"{$social['slug']}\"></a>";
	}
	echo "</div>";
?>