  <header class="fixed-top">
    <nav class="navbar">
      <div class="container">
        <a class="navbar-brand" href="<?=get_site_url()?>">
          <img src="http://ph4se.com.au/app/image/?text=[CLIENT_NAME]&w=300&h=80&size=20" alt="[CLIENT-NAME]">
        </a>
        <div id="navbar-toggle" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation" class="collapsed">
          <span></span><span></span><span></span>
        </div>
        <div class="navbar-collapse collapse justify-content-end" id="navbarCollapse" style="">
          <?php wp_nav_menu( array('theme_location' => 'primary', 'items_wrap' => '<ul class="navbar-nav">%3$s</ul>', 'container' => false) ); ?> 
        </div>
      </div>
    </nav>
  </header>