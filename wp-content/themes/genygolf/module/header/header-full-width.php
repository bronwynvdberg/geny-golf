    <div class="primary-nav">
      <div class="container-fluid">
        <nav class="navbar navbar-expand-lg navbar-light">
          <div class="row">
            <div class="col-10 col-sm-12">
              <a class="navbar-brand" href="<?php bloginfo('url'); ?>">
                <img src="http://ph4se.com.au/app/image/?text=Organisation Logo&w=300&h=80&size=20" class="img-fluid">
              </a>
            </div>
            <div class="col-2">
              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#primaryNav" aria-controls="primaryNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>
            </div>
          </div>
          <div class="row">
            <div class="col-12">
              <div class="collapse navbar-collapse" id="primaryNav">
              <?php wp_nav_menu( array('theme_location' => 'primary', 'items_wrap' => '<ul class="navbar-nav mr-auto">%3$s</ul>', 'container' => false, 'fallback_cb' => false) ); ?> 
              </div>
            </div>
          </div>
        </nav>
      </div>
    </div>