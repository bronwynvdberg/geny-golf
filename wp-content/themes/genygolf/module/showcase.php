<div class="showcase">
  <div class="container">
    <div class="showcasewrap">
      <div class="row">
        <?php
          $args = array( 'post_type' => 'showcase', 'posts_per_page' => 3);
          $loop = new WP_Query( $args );
          while ( $loop->have_posts() ) : $loop->the_post();
            $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
        ?>
        <div class="col-sm-4">
          <h3 class="text-center title"><?php the_title(); ?></h3>
          <a href="<?php  the_field('button_link'); ?>" class="showcaseimagewrap">
            <div class="showcaseimage" style="background-image:url(<?php echo $image[0]; ?>); "></div>
          </a>
          <a href="<?php the_field('button_link'); ?>" class="btn btn-primary"><?php the_field('button_text'); ?></a>
        </div>
        <?php
          endwhile;
          wp_reset_query();
        ?>
      </div>
    </div>
  </div>
</div>
