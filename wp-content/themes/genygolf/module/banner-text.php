<?php
	// Banner containing Heading
	// Uses $content from previous page > $content = Content::FromPost();
?>
		<div class="banner" style="background-image:url('<?=$content->image['url']?>');">
			<div class="container">
				<h1><?=embolden($content->title)?></h1>
				<?=$content->content?>
			</div>
		</div>
