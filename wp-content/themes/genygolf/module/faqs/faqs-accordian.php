<?php
	/*
			FAQ'S - ACCORDIAN
		
	*/
	$faqs = get_field('faqs','options');
	if($faqs){
		$i = 0;
		foreach($faqs as $faq){
			if($i == 0){
				$output .= "
					<div class=\"card\">
					    <div class=\"card-header\" id=\"faq_{$i}\">
					      <h5 class=\"mb-0\">
					        <button class=\"btn btn-link\" type=\"button\" data-toggle=\"collapse\" data-target=\"#collapse_{$i}\" aria-expanded=\"true\" aria-controls=\"collapse_{$i}\">
					          {$faq['question']}
					        </button>
					      </h5>
					    </div>

					    <div id=\"collapse_{$i}\" class=\"collapse show\" aria-labelledby=\"faq_{$i}\" data-parent=\"#accordion\">
				          <div class=\"card-body\">
				          	{$faq['answer']}
				          </div>
				        </div>
				      </div>
				";
			} else {
				$output .= "
					<div class=\"card\">
					    <div class=\"card-header\" id=\"faq_{$i}\">
					      <h5 class=\"mb-0\">
					        <button class=\"btn btn-link collapsed\" type=\"button\" data-toggle=\"collapse\" data-target=\"#collapse_{$i}\" aria-expanded=\"false\" aria-controls=\"collapse_{$i}\">
					          {$faq['question']}
					        </button>
					      </h5>
					    </div>

					    <div id=\"collapse_{$i}\" class=\"collapse\" aria-labelledby=\"faq_{$i}\" data-parent=\"#accordion\">
				          <div class=\"card-body\">
				          	{$faq['answer']}
				          </div>
				        </div>
				      </div>
				";
			}
			
			$i++;
		}

?>
		<div id="faqs">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="accordion" id="accordion">
							<?php echo $output; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
<?php
	} //end if $faqs
?>