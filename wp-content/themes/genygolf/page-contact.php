<?php
	/* Template Name: Page - Contact */

	get_header();
	$content = Content::FromPost();

	$offices = get_field('locations','options');
	$office = $offices[0];

	// CONTENT
?>
		<div class="content contact-layout">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<h1><?=embolden($content->title)?></h1>
						<div class="row justify-content-between">
							<div class="col-lg-6">
								<?=$content->content?>
								<div class="contact_details">
								<?php
									echo "
										<a href=\"tel:{$office['phone']}\">{$office['phone']}</a><br>
										<a href=\"mailto:{$office['email']}\">{$office['email']}</a><br>
										{$office['address']['address']}
									";
								?>
								</div>
								<?php include('module/social.php'); ?>
							</div>
							<div class="col-lg-5">
				        <?php echo do_shortcode('[gravityform id="1" title="false" description="false" ajax="false"]'); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="google-map">
	  	<div class="acf-map">
				<div class="marker" data-lat="<?=$office['address']['lat']?>" data-lng="<?=$office['address']['lng']?>"></div>
  		</div>
		</div>

	</div>
<?php get_footer(); ?>
