<?php
	/* Template Name: Page - Account Dashboard */
	get_header();
	$content = Content::FromPost();

	// CONTENT
?>

		<div class="content default-layout">
			<div class="container-fluid">
				<div class="row">
					<div class="col-2">
			            <?php wp_nav_menu( array('theme_location' => 'memberaccount', 'items_wrap' => '<ul class="navbar-nav nav flex-column ">%3$s</ul>', 'container' => false, 'fallback_cb' => false) ); ?> 
			        </div>
					<div class="col-10">
						<?=get_field('content')?>
						<?php
							if ( have_posts() ) while ( have_posts() ) : the_post();
								the_content();
							endwhile;
						?>
						<div class="col-10">
							WELCOME SECTION
						</div>
						<div class="col-2">
							LEADERBOARD
						</div>
					</div>

				</div>
			</div>
		</div>

	
	</div>
<?php get_footer(); ?>