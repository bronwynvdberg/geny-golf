module.exports = function(grunt) {
	
	// Configure tasks
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		uglify: {
			dist: {
				src: 'src/js/*.js',
				dest:'assets/js/script.min.js'
			},
			dev: {
				options: {
					beautify: true,
					mangle: false,
					compress: false,
					preserveComments: 'all'
				},
				src: 'src/js/*.js',
				dest:'assets/js/script.min.js'
			}
		},
		sass: {
			dev: {
				options: {
					outputStyle: 'expanded'
				},
				files: {
					'style.css' : 'src/scss/application.scss'
				}
			},
			dist: {
				options: {
					outputStyle: 'compressed'
				},
				files: {
					'style.css' : 'src/scss/application.scss'
				}
			}
		},
		watch: {
			js: {
				files: ['src/js/*.js'],
				tasks: ['uglify:dev']
			},
			css: {
				files: ['src/scss/**/*.scss'],
				tasks: ['sass:dev']
			}
		}
	});

	// Load plug ins
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-sass');

	// Register tasks
	grunt.registerTask('default', ['uglify:dev', 'sass:dev']);
	grunt.registerTask('dist', ['uglify:dist', 'sass:dist']);
}