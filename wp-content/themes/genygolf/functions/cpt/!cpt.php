<?php /** Custom Post Type **/
	add_action( 'init', 'create_post_type' );
	function create_post_type() {

		// SLIDER
		//
		// register_post_type( 'slider',
		// 	[
		// 		'labels'          => [
		// 			'name'          => __( 'Slider' ),
		// 			'singular_name' => __( 'Slider' ),
		// 			'add_new'       => _x( 'Add New', 'Slider' ),
		// 			'add_new_item'  => __( 'Add New Slider' ),
		// 			'edit_item'     => __( 'Edit Slider' ),
		// 			'new_item'      => __( 'New Slider' ),
		// 			'all_items'     => __( 'All Slider' )
		// 		],
		// 		'public'          => true,
		// 		'has_archive'     => false,
		// 		'menu_icon'       => 'dashicons-images-alt2',
		// 		'supports'        => [ 'title', 'editor', 'thumbnail' ]
		// 	]
		// );

		// SERVICES
		//
		register_post_type( 'service',
			[
				'labels'          => [
					'name'          => __( 'Services' ),
					'singular_name' => __( 'Service' ),
					'add_new'       => _x( 'Add New', 'Service' ),
					'add_new_item'  => __( 'Add New Service' ),
					'edit_item'     => __( 'Edit Service' ),
					'new_item'      => __( 'New Service' ),
					'all_items'     => __( 'All Services' )
				],
				'public'          => true,
				'has_archive'     => true,
				'menu_icon'       => 'dashicons-admin-tools',
 	    		'taxonomies'      => [ 'category' ],
				'supports'        => [ 'title', 'thumbnail' ]
			]
		);

		// PRODUCTS
		//
		register_post_type( 'product',
			[
				'labels'          => [
					'name'          => __( 'Products' ),
					'singular_name' => __( 'Product' ),
					'add_new'       => _x( 'Add New', 'Product' ),
					'add_new_item'  => __( 'Add New Product' ),
					'edit_item'     => __( 'Edit Product' ),
					'new_item'      => __( 'New Product' ),
					'all_items'     => __( 'All Products' )
				],
				'public'          => true,
				'has_archive'     => false,
				'menu_icon'       => 'dashicons-admin-generic',
				'supports'        => [ 'title', 'thumbnail' ]
			]
		);


	}
?>