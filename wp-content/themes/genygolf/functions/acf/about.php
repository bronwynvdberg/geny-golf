<?php
/*
		Advanced Custom Fields
		- ABOUT PAGE TEMPLATE

*/
	if(function_exists("register_field_group")) {
		acf_add_local_field_group(array(
			'key' => 'group_about_page',
			'title' => 'About',
			'fields' => array(
				//
				// About Header Panel
				//
				array(
					'key' => 'about-header-panel-equal-split-tab',
					'label' => 'Header Panel - Equal Split',
					'name' => '',
					'type' => 'tab',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'placement' => 'left',
					'endpoint' => 0,
				),
				array(
					'key' => 'about-header-panel-equal-split-title-tag',
					'label' => 'Title Style',
					'name' => 'about-header-panel_es_title_tag',
					'type' => 'select',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'choices' => array(
						'module_title' => 'Module title',
						'h1' => 'Header 1',
						'h2' => 'Header 2',
						'h3' => 'Header 3',
						'h4' => 'Header 4',
					),
					'default_value' => array(
					),
					'allow_null' => 0,
					'multiple' => 0,
					'ui' => 0,
					'ajax' => 0,
					'return_format' => 'value',
					'placeholder' => '',
				),
				array(
					'key' => 'about-header-panel-equal-split-title',
					'label' => 'Title',
					'name' => 'about-header-panel_es_title',
					'type' => 'textarea',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'maxlength' => '',
					'rows' => '',
					'new_lines' => 'br',
				),
				array(
					'key' => 'about-header-panel-equal-split-content',
					'label' => 'Content',
					'name' => 'about-header-panel_es_content',
					'type' => 'wysiwyg',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'tabs' => 'all',
					'toolbar' => 'full',
					'media_upload' => 1,
					'delay' => 0,
				),
				array(
					'key' => 'about-header-panel-equal-split-image',
					'label' => 'Background Image',
					'name' => 'about-header-panel_es_image',
					'type' => 'image',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'return_format' => 'array',
					'preview_size' => 'thumbnail',
					'library' => 'all',
					'min_width' => '',
					'min_height' => '',
					'min_size' => '',
					'max_width' => '',
					'max_height' => '',
					'max_size' => '',
					'mime_types' => '',
				),
				array(
					'key' => 'about-header-panel-equal-split-width',
					'label' => 'Module Width',
					'name' => 'about-header-panel_es_module_width',
					'type' => 'select',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'choices' => array(
						'default' => 'Container',
						'full-width' => 'Full Width',
					),
					'default_value' => array(
					),
					'allow_null' => 0,
					'multiple' => 0,
					'ui' => 0,
					'ajax' => 0,
					'return_format' => 'value',
					'placeholder' => '',
				),
				array(
					'key' => 'about-header-panel-equal-split-position',
					'label' => 'Content Position',
					'name' => 'about-header-panel_es_position',
					'type' => 'select',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'choices' => array(
						'es-left' => 'Left',
						'es-right' => 'Right',
					),
					'default_value' => array(
					),
					'allow_null' => 0,
					'multiple' => 0,
					'ui' => 0,
					'ajax' => 0,
					'return_format' => 'value',
					'placeholder' => '',
				),
				array(
					'key' => 'about-header-panel-equal-split-background',
					'label' => 'Content Background',
					'name' => 'about-header-panel_es_background',
					'type' => 'select',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'choices' => array(
						'no-bg' => 'No Colour',
						'grad_primary_lr' => 'Gradient Primary - Left to Right',
						'grad_primary_rl' => 'Gradient Primary - Right to Left',
						'grad_secondary_lr' => 'Gradient Secondary - Left to Right',
						'grad_secondary_rl' => 'Gradient Secondary - Right to Left',
						'bg_primary' => 'Solid Background - Primary Colour',
						'bg_secondary' => 'Solid Background - Secondary Colour',
						'bg_tertiary' => 'Solid Background - Tertiary Colour',
						'solid' => 'Solid Colour',
					),
					'default_value' => array(
					),
					'allow_null' => 0,
					'multiple' => 0,
					'ui' => 0,
					'ajax' => 0,
					'return_format' => 'value',
					'placeholder' => '',
				),
				array(
					'key' => 'about-header-panel-equal-split-font',
					'label' => 'Font Colour',
					'name' => 'about-header-panel_es_font',
					'type' => 'color_picker',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => array(
						array(
							array(
								'field' => 'about-header-panel-equal-split-background',
								'operator' => '==',
								'value' => 'solid',
							),
						),
					),
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '#333',
				),
				array(
					'key' => 'about-header-panel-equal-split-colour',
					'label' => 'Solid Colour',
					'name' => 'about-header-panel_es_colour',
					'type' => 'color_picker',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => array(
						array(
							array(
								'field' => 'about-header-panel-equal-split-background',
								'operator' => '==',
								'value' => 'solid',
							),
						),
					),
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '#F1F1F1',
				),
				//
				// About Panel 1 - Content Panel
				//
				array(
					'key' => 'about-panel-1-content-panel-tab',
					'label' => 'Panel 1 - Content Panel',
					'name' => '',
					'type' => 'tab',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'placement' => 'left',
					'endpoint' => 0,
				),
				array(
					'key' => 'about-panel-1-cp-title-tag',
					'label' => 'Title Style',
					'name' => 'about-panel-1_cp_title_tag',
					'type' => 'select',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'choices' => array(
						'module_title' => 'Module title',
						'h1' => 'Header 1',
						'h2' => 'Header 2',
						'h3' => 'Header 3',
						'h4' => 'Header 4',
					),
					'default_value' => array(
					),
					'allow_null' => 0,
					'multiple' => 0,
					'ui' => 0,
					'ajax' => 0,
					'return_format' => 'value',
					'placeholder' => '',
				),
				array(
					'key' => 'about-panel-1-cp-title',
					'label' => 'Title',
					'name' => 'about-panel-1_cp_title',
					'type' => 'textarea',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'maxlength' => '',
					'rows' => '',
					'new_lines' => '',
				),
				array(
					'key' => 'about-panel-1-cp-intro-paragraph',
					'label' => 'Introduction Paragraph',
					'name' => 'about-panel-1_cp_intro_paragraph',
					'type' => 'wysiwyg',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'tabs' => 'all',
					'toolbar' => 'full',
					'media_upload' => 1,
					'delay' => 0,
				),
				array(
					'key' => 'about-panel-1-cp-content',
					'label' => 'Content',
					'name' => 'about-panel-1_cp_content',
					'type' => 'wysiwyg',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'tabs' => 'all',
					'toolbar' => 'full',
					'media_upload' => 1,
					'delay' => 0,
				),
				//
				// About Panel 2 - Call To Action
				//
				array(
					'key' => 'about-panel-2-cta-tab',
					'label' => 'Panel 2 - CTA',
					'name' => '',
					'type' => 'tab',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'placement' => 'left',
					'endpoint' => 0,
				),
				array(
					'key' => 'about-panel-2-cta-title-tag',
					'label' => 'Title Style',
					'name' => 'about-panel-2_cta_title_tag',
					'type' => 'select',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'choices' => array(
						'module_title' => 'Module title',
						'h1' => 'Header 1',
						'h2' => 'Header 2',
						'h3' => 'Header 3',
						'h4' => 'Header 4',
					),
					'default_value' => array(
					),
					'allow_null' => 0,
					'multiple' => 0,
					'ui' => 0,
					'ajax' => 0,
					'return_format' => 'value',
					'placeholder' => '',
				),
				array(
					'key' => 'about-panel-2-cta-title',
					'label' => 'Title',
					'name' => 'about-panel-2_cta_title',
					'type' => 'textarea',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'maxlength' => '',
					'rows' => '',
					'new_lines' => '',
				),
				array(
					'key' => 'about-panel-2-cta-content',
					'label' => 'Content',
					'name' => 'about-panel-2_cta_content',
					'type' => 'textarea',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'maxlength' => '',
					'rows' => '',
					'new_lines' => '',
				),
				array(
					'key' => 'about-panel-2-cta-button-text',
					'label' => 'Button Text',
					'name' => 'about-panel-2_cta_button_text',
					'type' => 'text',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
				),
				array(
					'key' => 'about-panel-2-cta-button-link',
					'label' => 'Button Link',
					'name' => 'about-panel-2_cta_button_link',
					'type' => 'text',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
				),
				array(
					'key' => 'about-panel-2-cta-button-style',
					'label' => 'Button Style',
					'name' => 'about-panel-2_cta_button_style',
					'type' => 'select',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'choices' => array(
						'btn' => 'Default',
						'btn btn-primary' => 'Primary',
						'btn btn-secondary' => 'Secondary',
						'btn btn-tertiary' => 'Tertiary',
					),
					'default_value' => array(
					),
					'allow_null' => 0,
					'multiple' => 0,
					'ui' => 0,
					'ajax' => 0,
					'return_format' => 'value',
					'placeholder' => '',
				),
				array(
					'key' => 'about-panel-2-cta-width',
					'label' => 'Module Width',
					'name' => 'about-panel-2_cta_module_width',
					'type' => 'select',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'choices' => array(
						'default' => 'Container',
						'full-width' => 'Full Width',
					),
					'default_value' => array(
					),
					'allow_null' => 0,
					'multiple' => 0,
					'ui' => 0,
					'ajax' => 0,
					'return_format' => 'value',
					'placeholder' => '',
				),
				array(
					'key' => 'about-panel-2-cta-position',
					'label' => 'Content Position',
					'name' => 'about-panel-2_cta_position',
					'type' => 'select',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'choices' => array(
						'es-left' => 'Left',
						'es-centre' => 'Centre',
						'es-right' => 'Right',
					),
					'default_value' => array(
					),
					'allow_null' => 0,
					'multiple' => 0,
					'ui' => 0,
					'ajax' => 0,
					'return_format' => 'value',
					'placeholder' => '',
				),
				array(
					'key' => 'about-panel-2-cta-background',
					'label' => 'Content Background',
					'name' => 'about-panel-2_cta_background',
					'type' => 'select',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'choices' => array(
						'no-bg' => 'No Colour',
						'grad_primary_lr' => 'Gradient Primary - Left to Right',
						'grad_primary_rl' => 'Gradient Primary - Right to Left',
						'grad_secondary_lr' => 'Gradient Secondary - Left to Right',
						'grad_secondary_rl' => 'Gradient Secondary - Right to Left',
						'bg_primary' => 'Solid Background - Primary Colour',
						'bg_secondary' => 'Solid Background - Secondary Colour',
						'bg_tertiary' => 'Solid Background - Tertiary Colour',
						'solid' => 'Solid Colour',
					),
					'default_value' => array(
					),
					'allow_null' => 0,
					'multiple' => 0,
					'ui' => 0,
					'ajax' => 0,
					'return_format' => 'value',
					'placeholder' => '',
				),
				array(
					'key' => 'about-panel-2-cta-colour',
					'label' => 'Solid Colour',
					'name' => 'about-panel-2_cta_colour',
					'type' => 'color_picker',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => array(
						array(
							array(
								'field' => 'about-panel-2-cta-background',
								'operator' => '==',
								'value' => 'solid',
							),
						),
					),
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '#F1F1F1',
				),
				//
				// About Panel 3
				//
				array(
					'key' => 'about-panel-3-equal-split-tab',
					'label' => 'Panel 3 - Equal Split',
					'name' => '',
					'type' => 'tab',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'placement' => 'left',
					'endpoint' => 0,
				),
				array(
					'key' => 'about-panel-3-equal-split-title-tag',
					'label' => 'Title Style',
					'name' => 'about-panel-3_es_title_tag',
					'type' => 'select',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'choices' => array(
						'module_title' => 'Module title',
						'h1' => 'Header 1',
						'h2' => 'Header 2',
						'h3' => 'Header 3',
						'h4' => 'Header 4',
					),
					'default_value' => array(
					),
					'allow_null' => 0,
					'multiple' => 0,
					'ui' => 0,
					'ajax' => 0,
					'return_format' => 'value',
					'placeholder' => '',
				),
				array(
					'key' => 'about-panel-3-equal-split-title',
					'label' => 'Title',
					'name' => 'about-panel-3_es_title',
					'type' => 'textarea',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'maxlength' => '',
					'rows' => '',
					'new_lines' => 'br',
				),
				array(
					'key' => 'about-panel-3-equal-split-content',
					'label' => 'Content',
					'name' => 'about-panel-3_es_content',
					'type' => 'wysiwyg',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'tabs' => 'all',
					'toolbar' => 'full',
					'media_upload' => 1,
					'delay' => 0,
				),
				array(
					'key' => 'about-panel-3-equal-split-image',
					'label' => 'Background Image',
					'name' => 'about-panel-3_es_image',
					'type' => 'image',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'return_format' => 'array',
					'preview_size' => 'thumbnail',
					'library' => 'all',
					'min_width' => '',
					'min_height' => '',
					'min_size' => '',
					'max_width' => '',
					'max_height' => '',
					'max_size' => '',
					'mime_types' => '',
				),
				array(
					'key' => 'about-panel-3-equal-split-width',
					'label' => 'Module Width',
					'name' => 'about-panel-3_es_module_width',
					'type' => 'select',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'choices' => array(
						'default' => 'Container',
						'full-width' => 'Full Width',
					),
					'default_value' => array(
					),
					'allow_null' => 0,
					'multiple' => 0,
					'ui' => 0,
					'ajax' => 0,
					'return_format' => 'value',
					'placeholder' => '',
				),
				array(
					'key' => 'about-panel-3-equal-split-position',
					'label' => 'Content Position',
					'name' => 'about-panel-3_es_position',
					'type' => 'select',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'choices' => array(
						'es-left' => 'Left',
						'es-right' => 'Right',
					),
					'default_value' => array(
					),
					'allow_null' => 0,
					'multiple' => 0,
					'ui' => 0,
					'ajax' => 0,
					'return_format' => 'value',
					'placeholder' => '',
				),
				array(
					'key' => 'about-panel-3-equal-split-background',
					'label' => 'Content Background',
					'name' => 'about-panel-3_es_background',
					'type' => 'select',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'choices' => array(
						'no-bg' => 'No Colour',
						'grad_primary_lr' => 'Gradient Primary - Left to Right',
						'grad_primary_rl' => 'Gradient Primary - Right to Left',
						'grad_secondary_lr' => 'Gradient Secondary - Left to Right',
						'grad_secondary_rl' => 'Gradient Secondary - Right to Left',
						'bg_primary' => 'Solid Background - Primary Colour',
						'bg_secondary' => 'Solid Background - Secondary Colour',
						'bg_tertiary' => 'Solid Background - Tertiary Colour',
						'solid' => 'Solid Colour',
					),
					'default_value' => array(
					),
					'allow_null' => 0,
					'multiple' => 0,
					'ui' => 0,
					'ajax' => 0,
					'return_format' => 'value',
					'placeholder' => '',
				),
				array(
					'key' => 'about-panel-3-equal-split-font',
					'label' => 'Font Colour',
					'name' => 'about-panel-3_es_font',
					'type' => 'color_picker',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => array(
						array(
							array(
								'field' => 'about-panel-3-equal-split-background',
								'operator' => '==',
								'value' => 'solid',
							),
						),
					),
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '#333',
				),
				array(
					'key' => 'about-panel-3-equal-split-colour',
					'label' => 'Solid Colour',
					'name' => 'about-panel-3_es_colour',
					'type' => 'color_picker',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => array(
						array(
							array(
								'field' => 'about-panel-3-equal-split-background',
								'operator' => '==',
								'value' => 'solid',
							),
						),
					),
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '#F1F1F1',
				),












				// End -----------------
			),
			'location' => array(
				array(
					array(
						'param' => 'page_template',
						'operator' => '==',
						'value' => 'page-about.php',
					),
				),
			),
			'menu_order' => 0,
			'position' => 'normal',
			'style' => 'default',
			'label_placement' => 'top',
			'instruction_placement' => 'label',
			'hide_on_screen' => array(
				0 => 'the_content',
				1 => 'featured_image',
			),
			'active' => 1,
			'description' => '',
		));

	}
?>