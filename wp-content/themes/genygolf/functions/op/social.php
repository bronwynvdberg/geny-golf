<?php
	/*
			SOCIAL MEDIA ACCOUNTS

	*/
	if( function_exists('acf_add_local_field_group') ) {
		acf_add_local_field_group(array(
			'key' => 'group_5aefd3dfc8403',
			'title' => 'Socials',
			'fields' => array(
				array(
					'key' => 'field_5aefd3ef42060',
					'label' => 'Socials',
					'name' => 'socials',
					'type' => 'repeater',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'collapsed' => '',
					'min' => 0,
					'max' => 0,
					'layout' => 'table',
					'button_label' => '',
					'sub_fields' => array(
						array(
							'key' => 'field_5aefd3f542061',
							'label' => 'Slug',
							'name' => 'slug',
							'type' => 'select',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'choices' => array(
								'p4-facebook' => 'Facebook',
								'p4-twitter' => 'Twitter',
								'p4-youtube' => 'YouTube',
								'p4-linked-in' => 'Lined In',
								'p4-snapchat' => 'Snapchat',
								'p4-instagram' => 'Instagram',
								'p4-pinterest' => 'Pinterest',
								'p4-envelope-1-light' => 'Mail 1',
								'p4-envelope-1-bold' => 'Mail 1 (bold)',
								'p4-envelope-2-light' => 'Mail 2',
								'p4-envelope-2-bold' => 'Mail 2 (bold)',
							),
							'default_value' => array(
							),
							'allow_null' => 0,
							'multiple' => 0,
							'ui' => 0,
							'ajax' => 0,
							'return_format' => 'value',
							'placeholder' => '',
						),
						array(
							'key' => 'field_5aefd49642062',
							'label' => 'Url',
							'name' => 'url',
							'type' => 'text',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'default_value' => '#',
							'placeholder' => '',
							'prepend' => '',
							'append' => '',
							'maxlength' => '',
						),
					),
				),
			),
			'location' => array(
				array(
					array(
						'param' => 'options_page',
						'operator' => '==',
						'value' => 'theme-options-socials',
					),
				),
			),
			'menu_order' => 0,
			'position' => 'normal',
			'style' => 'default',
			'label_placement' => 'top',
			'instruction_placement' => 'label',
			'hide_on_screen' => '',
			'active' => 1,
			'description' => '',
		));
	}
?>