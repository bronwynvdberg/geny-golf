<?php
  /*
      WIDGETS
  
  */

  // Sidebars
  //
  function sbm_widgets_init() {
    register_sidebar([
      'name'          => __( 'Header', 'sbm' ),
      'id'            => 'header',
      'before_widget' => '<div class="widget panel panel-default panel-%1$s %2$s">',
      'after_widget'  => '</div></div>',
      'before_title'  => '<div class="panel-heading"><h3 class="panel-title">',
      'after_title'   => '</h3></div><div class="panel-body">',
    ]);
    register_sidebar([
      'name'          => __( 'Sidebar', 'sbm' ),
      'id'            => 'sidebar',
      'before_widget' => '<div class="panel panel-default panel-%1$s %2$s">',
      'after_widget'  => '</div></div>',
      'before_title'  => '<div class="panel-heading"><h3 class="panel-title">',
      'after_title'   => '</h3></div><div class="panel-body">',
    ]);
    register_sidebar([
      'name'          => __( 'Showcase', 'sbm' ),
      'id'            => 'showcase',
      'before_widget' => '<div class="col-sm-4"><div class="panel panel-default panel-%1$s %2$s">',
      'after_widget'  => '</div></div></div>',
      'before_title'  => '<div class="panel-heading"><h3 class="panel-title">',
      'after_title'   => '</h3></div><div class="panel-body">',
    ]);
  }
  add_action( 'widgets_init', 'sbm_widgets_init' );

  // Widget Content
  //
  class quick_contact_widget extends WP_Widget {
    // php classnames and widget name/description added
    function __construct() {
      $widget_options = array(
        'classname' => 'quick_contact_widget',
        'description' => 'Quick Contact'
      );
      parent::__construct( 
        'quick_contact_widget', 
        ' Quick Contact', 
        $widget_options 
      );
    }

    // create the widget output
    function widget( $args, $instance ) {
      $title = "";
      $summary =  $instance[ 'summary' ];
      $phone =  $instance[ 'phone' ] ;

      echo $args['before_widget'] . $args['before_title'] . "" . $args['after_title'];
      ?>
      <div class="widget-contact">
        <span class="phone-number">
          <i class="fa fa-phone"></i>
          <span class="number">
            <a href="tel:<?=$phone?>"" class="h1 ch"><?=$phone?></a>
          </span>
        </span>
        <span class="summary"><?=$summary?></span>
            
      </div>
      <?php
      echo $args['after_widget'];
    }

    function form( $instance ) { 
      $title = ! empty( $instance['title'] ) ? $instance['title'] : '';
      $summary = ! empty( $instance['summary'] ) ? $instance['summary'] : ''; 
      $phone = ! empty( $instance['phone'] ) ? $instance['phone'] : '';
      ?>
      <p>
        <label for="<?php echo $this->get_field_id( 'summary' ); ?>">Summary:
        <textarea class="widefat" type="text" id="<?php echo $this->get_field_id( 'summary' ); ?>" name="<?php echo $this->get_field_name( 'summary' ); ?>"><?php echo esc_textarea( $instance['summary'] ); ?></textarea></label>
      </p>
      <p>
        <label for="<?php echo $this->get_field_id('phone'); ?>"><?php _e('Phone Number:'); ?>
        <input class="widefat" id="<?php echo $this->get_field_id('phone'); ?>" name="<?php echo $this->get_field_name('phone'); ?>" type="text" value="<?php echo esc_attr($phone); ?>" /></label>
      </p>
    <?php
    }

    // Update database with new info
    function update( $new_instance, $old_instance ) { 
      $instance = $old_instance;
      $instance['title'] = strip_tags( $new_instance[ 'title' ] );
      $instance['summary'] = strip_tags( $new_instance[ 'summary' ] );
      $instance['phone'] = strip_tags( $new_instance[ 'phone' ] );
      return $instance;
    }
  }

  function jpen_register_widgets() { 
    register_widget( 'quick_contact_widget' );
  }
  add_action( 'widgets_init', 'jpen_register_widgets' );
?>