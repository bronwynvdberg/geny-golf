<?php
	/*
			Navigation

	*/
	// Menus - Register //
	register_nav_menus( array(
		'primary' => __( 'Primary Navigation', 'sbm' ),
		'memberaccount' => __( 'Account Navigation', 'sbm' ),
		'footer' => __( 'Footer Navigation', 'sbm' ),
	) );

	// Menus - Add Active Class //

	function current_to_active($text){
		$replace = array(
			'current_page_item' 			=> 'active',
			'current_page_parent' 			=> 'active',
			'current_page_ancestor' 		=> 'active',
			'menu-item'						=> 'nav-item'
		);
		$text = str_replace(array_keys($replace), $replace, $text);
			return $text; 
	}
	add_filter ('wp_nav_menu','current_to_active');

	function add_current_nav_class( $classes, $item ) {

		// Getting the current post details
		global $post;

		// Getting the post type of the current post
		$current_post_type = get_post_type_object( get_post_type( $post->ID ) );
		$current_post_type_slug = $current_post_type->rewrite[ 'slug' ];

		// Getting the URL of the menu item

		$menu_slug = strtolower( trim( $item->url ) );

		// Unset current page parent css for non post pages
		$post_type = get_post_type( $post );

		if ( $post_type != 'post' ) {		
			$index = array_search( 'current_page_parent', $classes );
			unset( $classes[ $index ] );
		}

		// If the menu item URL contains the current post types slug add the current-menu-item class
		if ( strpos( $menu_slug, $current_post_type_slug ) !== false ) {
			$classes[ ] = 'current_page_parent';
		}
		// Return the corrected set of classes to be added to the menu item
		return $classes;
	}
	add_action( 'nav_menu_css_class', 'add_current_nav_class', 10, 2 );

?>