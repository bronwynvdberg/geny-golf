<?php
	/* Template Name: Page - Full Width */
	get_header("full-width");
?>
		
		<!-- BANNER -->
	 	<?php include('module/banner-fixed-width.php'); ?>

		<!-- CONTENT -->
		<div class="content main">
			<div class="container">
				<div class="contentwrap">

					<div class="drop-shadow"><img src="<?php bloginfo('template_url'); ?>/assets/images/dropshadow.png" alt="" class="img-responsive"></div>
					<div class="col-sm-12 col-md-12">
						<div class="breadcrumbs"><?php if(function_exists('bcn_display')) { bcn_display(); }?></div>
						<!-- Loop -->
						<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
							<h1><?php the_title(); ?></h1>
							<?php the_content(); ?>
						<?php endwhile; ?>
					</div>

				</div>
			</div>
		</div>
	</div>
		
<?php get_footer(); ?>