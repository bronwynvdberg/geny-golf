<?php get_header(); ?>
				
		<!-- CONTENT -->
		
		<div class="content">
		<div class="container">

			<!-- BANNER -->
			
			<div class="banner">
						
				<h1>FAQ's</h1>
	
			</div><!--/.banner -->


		<div class="contentwrap">
		<div class="row">
				
			<div class="col-sm-8">
			<div class="main">
				 	
				 <div class="panel-group" id="accordion">
				 
				 <!-- Loop -->
				 <?php query_posts( array( 'post_type' => 'faqs', 'showposts' => 300, 'orderby' => 'title', 'order' => 'asc') );
				  if ( have_posts() ) : while ( have_posts() ) : the_post();
				?>
					<div class="panel">
					  <div class="panel-heading">
					    <h3 class="panel-title">
					      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php the_ID(); ?>">
					        <?php the_title(); ?>
					      </a>
					    </h3>
					  </div>
					  <div id="collapse<?php the_ID(); ?>" class="panel-collapse collapse">
					    <div class="panel-body">
					      <?php the_content(); ?>
					    </div>
					  </div>
					</div>
								
				<?php endwhile; endif; ?>
				<!-- END Loop -->
				
				</div>

			</div> 
			</div><!--/.main -->
			
			<div class="col-sm-4">
			<div class="aside">
				
				<?php get_sidebar(); ?>

			</div>			    
			</div><!--/.aside -->
				
			<div class="col-sm-12">
			<div class="footerlinks">
				
				<div class="row">
					<?php if ( ! dynamic_sidebar( 'footerlinks' ) ) : ?><?php endif; ?>
				</div><!--/.row -->
	
			</div>
			</div><!--/.footer -->

		</div><!--/.row -->
		</div><!--/.contentwrap -->
		</div><!--/.container -->
		</div><!--/.content -->
		
<?php get_footer(); ?>
