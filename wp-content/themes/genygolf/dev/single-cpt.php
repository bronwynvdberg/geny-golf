<?php get_header(); ?>
		
		<!-- BANNER -->
		
		<div class="banner">
		<div class="container">
		<div class="bannerwrap">
					
			<?php if(has_post_thumbnail()) :?>
			
			    <?php the_post_thumbnail('thumbnail', array('class' => 'img-responsive')); ?>
			
			<?php else : ?>
			    
			    <img src="<?php bloginfo('template_url'); ?>/assets/images/default-banner.jpg" class="img-responsive">
			
			<?php endif; ?>

		</div><!--/.bannerwrap -->
		</div><!--/.container -->
		</div><!--/.banner -->
		
		
		<!-- CONTENT -->
		
		<div class="content">
		<div class="container">
		<div class="contentwrap">
		<div class="row">
				
			<div class="col-sm-8">
			<div class="main">
			 	
				<!-- Loop -->
				<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
				
					<h1><?php the_title(); ?></h1>

					<?php the_content(); ?>
				
				<?php endwhile; ?>
				<!-- END Loop -->

			</div> 
			</div><!--/.main -->
			
			<div class="col-sm-4">
			<div class="aside">
				
				<?php get_sidebar(); ?>

			</div>			    
			</div><!--/.aside -->
				
		</div><!--/.row -->
		</div><!--/.contentwrap -->
		</div><!--/.container -->
		</div><!--/.content -->
		
<?php get_footer(); ?>