<?php

	// Hide Top Admin Panel
	add_filter('show_admin_bar', '__return_false');

	// Format navbar, wpadminbar and page content correctly //

	function admin_bar_offset() {
		echo"<style>@media (max-width:768px){body{margin-top:0px}</style>";

		if(is_admin_bar_showing()) {
			echo"<style>.navbar-fixed-top.visible-xs {top:0px;} @media (max-width:768px){ #wpadminbar {display:none !important;} html{margin-top:0 !important} }</style>";
		}
	}

	add_action('wp_head', 'admin_bar_offset');

	// Remove Default Gallery Style //

	add_filter( 'use_default_gallery_style', '__return_false' );

	// Enable Post Thumbnails //

	add_theme_support( 'post-thumbnails' );
	function theme_slug_setup() {
	   add_theme_support( 'title-tag' );
	}
	add_action( 'after_setup_theme', 'theme_slug_setup' );
	// Admin - Style //

	add_action('admin_head', 'trublue_admin_style');

	function trublue_admin_style() {
	echo '<style>
		#adminmenu {
		margin: 0;
		}
		#adminmenu div.separator {
		border-color: #444;
		}
		#adminmenu li.wp-menu-separator {
		margin: 0;
		}
	}
	</style>';
	}

	add_action('admin_bar_menu', 'trublue_admin_bar');

	function trublue_admin_bar() {
	echo '<style>
		#wpadminbar {
		background: #000;
		}
		#wpadminbar #wp-admin-bar-wp-logo,
		#wpadminbar #wp-admin-bar-comments,
		#wpadminbar #wp-admin-bar-wpseo-menu,
		#wpadminbar #wp-admin-bar-slideshows-top_menu {
		display:none;
		}
	}
	</style>';
	}

	add_action('login_head', 'trublue_login_logo');
	function trublue_login_logo() {
    echo '<style type="text/css">
		.login h1 a {
		background-image:url('.get_stylesheet_directory_uri().'/assets/admin/SBM_logo.png) !important;
		background-size: 200px !important;
    	width: 100%;
    	height:110px;
    	padding-bottom: 0 !important;
		background-position: 50% 50%;
		}
    </style>';
	}

	add_filter('login_headerurl', 'trublue_login_url');
	function trublue_login_url(){
    	return get_bloginfo( 'wpurl' );
	}

	add_filter('admin_footer_text', 'rvam_modify_footer_admin');
	function rvam_modify_footer_admin ()
	{
	    echo '<span id="footer-thankyou">Website developed by <a href="http://www.smithbrothersmedia.com.au" target="_blank">Smith Brothers Media </a> | For problems please send a <a href="mailto:support@smithbrothersmedia.com.au?subject=Support Request&body=For support requests please make sure you let us know your name and contact detains, the domain name of the site, and a detailed description of the problem. Thank you." target="_blank">Support Request</a></span>';
	}

	add_filter ( 'login_errors', 'better_failed_login' );
	function better_failed_login () {
	    return 'The login information you have entered is incorrect. Please try again.';
	}

	// Admin - Redirect non-admin's //

	function nonadmin_login_redirect( $redirect_to, $request, $user  ) {
		return ( is_array( $user->roles ) && in_array( 'administrator', $user->roles ) ) ? admin_url() : site_url();
	}

	add_filter( 'login_redirect', 'nonadmin_login_redirect', 10, 3 );

	// Admin - Remove Menu Items //

	function remove_menus(){
		remove_menu_page( 'edit-comments.php' );
		remove_menu_page( 'index.php' );
	}

	add_action( 'admin_menu', 'remove_menus' );

	// Change 'Posts' to 'News'
	//
	function sbm_change_post_label() {
	  global $menu;
	  global $submenu;
	  $menu[5][0] = 'News';
	  $submenu['edit.php'][5][0] = 'News';
	  $submenu['edit.php'][10][0] = 'Add News';
	  $submenu['edit.php'][16][0] = 'News Tags';
	}
	function sbm_change_post_object() {
	  global $wp_post_types;
	  $labels = &$wp_post_types['post']->labels;
	  $labels->name = 'News';
	  $labels->singular_name = 'News';
	  $labels->add_new = 'Add News';
	  $labels->add_new_item = 'Add News';
	  $labels->edit_item = 'Edit News';
	  $labels->new_item = 'News';
	  $labels->view_item = 'View News';
	  $labels->search_items = 'Search News';
	  $labels->not_found = 'No News found';
	  $labels->not_found_in_trash = 'No News found in Trash';
	  $labels->all_items = 'All News';
	  $labels->menu_name = 'News';
	  $labels->name_admin_bar = 'News';
	}

	add_action( 'admin_menu', 'sbm_change_post_label' );
	add_action( 'init', 'sbm_change_post_object' );

	// CPT showing 404
	// flush_rewrite_rules( false );


	
	/*
			INCLUDE FILES
			- Load all files located in the relevant folders
	*/

// echo $_SERVER['DOCUMENT_ROOT']."/megaltd/public_html/";

	$dir = dirname( __FILE__ )."/functions/";
	if (is_dir($dir)) {
		$load_folders = ['custom','cpt','acf','op'];
		$blacklist    = ['.', '..', '.DS_Store', '_temp', 'index.php', 'index.html'];
		foreach ($load_folders as $folder) {
			$files = scandir("{$dir}{$folder}");
			foreach ($files as $file) {
				if (!in_array($file, $blacklist)) {
					include_once "functions/{$folder}/{$file}";
				}
			}
		}
	}
?>
