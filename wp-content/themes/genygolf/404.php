<?php
	get_header(); 

	include("module/banner.php");

?>

			
		<div class="content">
		<div class="container">
		<div class="contentwrap">
		<div class="row">
			
			<div class="col-sm-8">
			<div class="main">

				<!-- <div class="breadcrumbs"><?php if(function_exists('bcn_display')) { bcn_display(); }?></div> -->
				
				<h1>404: Page not Found</h1>
				
				<p>Sorry, but the page you are looking for has not been found. Try checking the URL for errors, then hit the refresh button on your browser. Alternatively select one of the links above.</p>
			
			</div> 
			</div><!--/.main -->
			
			<div class="col-sm-4">
			<div class="aside">
				
				<?php get_sidebar(); ?>
				
			</div>			    
			</div><!--/.aside -->
				
		</div><!--/.row -->
		</div><!--/.contentwrap -->
		</div><!--/.container -->
		</div><!--/.content -->
		
<?php get_footer(); ?>