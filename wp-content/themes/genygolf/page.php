<?php
	get_header();
	$content = Content::FromPost();

	// Banner
	include('module/banner.php');


	// CONTENT
?>
		<div class="content default-layout">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<?=get_field('introduction_text')?>
					</div>
					<div class="col-lg-6">
						<?php include('module/carousel/carousel-dirty.php'); ?>
					</div>
					<div class="col-lg-6">
						<?=get_field('content')?>
						<?php
							if ( have_posts() ) while ( have_posts() ) : the_post();
								the_content();
							endwhile;
						?>
					</div>

				</div>
			</div>
		</div>

	
	</div>
<?php get_footer(); ?>