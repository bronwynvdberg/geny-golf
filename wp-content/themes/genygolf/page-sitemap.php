<?php
/*
* Template Name: Page - Sitemap
*/
get_header(); ?>


		<!-- CONTENT -->

		<div class="content">
		<div class="container">
		<div class="contentwrap">
		<div class="row">
			<h1 class="text-center"> Sitemap </h1>
			<br/>
			<div class="col-sm-3"></div>

				<div class="col-sm-6">
		<?php get_search_form(); ?>
			</div>
				<div class="col-sm-3"></div>
				<div class="clearfix"></div>
				<br />
<br />
				<hr>
								<div class="clearfix"></div>
			<div class="main">

<div class="col-sm-4">
				<!-- PAGES -->
				<h3>Pages</h3>
				<?php wp_list_pages(array('sort_column' => 'post_date', 'title_li' => '', 'sort_order' => 'asc')); ?>

				<!-- PAGES END -->
			</div>
				<div class="col-sm-4">
 <!-- POSTS -->
 <?php
 $categories = get_categories( array(
     'orderby' => 'name',
     'order'   => 'ASC',
		 'parent' => 0
 ) ); ?>
<h3>Blog Posts</h3>
<ul>
<?php
 foreach( $categories as $category ) {
           $categoryName = $category->name;
					 $categoryID = get_cat_ID( $categoryName );
					 $parentCategoryName = get_category_parents( $categoryID, true, ''); ?>
<li>
	<?php echo $parentCategoryName; ?>
	<ul>
			<?php
				$childCategories =  get_categories( array(
					'child_of' =>  $categoryID,
		      'orderby' => 'name',
		      'order'   => 'ASC'
		  	));

				foreach( $childCategories as $childCategory ) {
									$childCategoryName = $childCategory->name;
									$childCategoryID = get_cat_ID($childCategoryName);
									$childCategoryLink = get_category_link($childCategoryID );
					?>
									<li><a href="<?php echo $childCategoryLink; ?>"><?php echo $childCategoryName; ?></a>
											<ul>
												<?php query_posts( array( 'post_type' => 'post', 'order' => 'asc', 'post_status'=> 'publish', 'cat' => $childCategoryID) );
												 if ( have_posts() ) : while ( have_posts() ) : the_post();
											 ?>
											 <li>
												 <a href="<?php the_permalink() ?>" ><?php the_title(); ?></a>
											 </li>

											 <?php endwhile; endif; ?>
											</ul>
									</li>
			<?php } ?>
	</ul>
</li>

 <?php } ?>

</ul>

 <!-- POSTS END -->


	</div>
	<div class="col-sm-4">
<!-- CUSTOM POST TYPE -->


<!-- CUSTOM POST TYPE END-->
	</div>
			</div><!--/.main -->


		</div><!--/.row -->
		</div><!--/.contentwrap -->
		</div><!--/.container -->
		</div><!--/.content -->

<?php get_footer(); ?>
