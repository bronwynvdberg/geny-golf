
--== SBM-Framework ==--
> template to redirect users accessing attachment template for images and files. to be used for cpt's that do not require individual post templates or pages(slider etc.)

> Sticky Footer
  # The page wrapper needs to have a 'padding-bottom' which matches the footer 'height' and 'margin-top'.
  @ Adjust:
    - '.wrap' > 'padding-bottom'
    - '.footer' > 'margin-top'
    - '.footer .container' > 'height'

> When pushing live, make sure the following files are not include:
  - gruntfile.js
  - package-lock.json
  - package.json

> When pushing from local, staging - change rewrite info in .htaccess(RewriteBase & RewriteRule-last one)

