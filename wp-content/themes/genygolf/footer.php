<?php
	// CacheBuster
	$buster = time();

	// FOOTER -------------------------------------------------------------------------
	$offices = get_field('locations','options');
	$office = $offices[0];

?>
	<footer>
		<div class="container">
			<div class="row">
				<div class="order-1 col-lg-2">
    	    <a class="footer-brand" href="<?=get_site_url()?>">
  	        <img src="http://ph4se.com.au/app/image/?text=[CLIENT_NAME]&w=300&h=80&size=20" alt="[CLIENT-NAME]">
	        </a>
				</div>
				<div class="order-2 col-lg-2">
					<a class="back-top d-lg-none" href="#top">
						<img class="svg" src="<?php bloginfo('template_url'); ?>/assets/images/Back_to_top.svg?version=<?=buster?>" width="50" height="50" alt="Back to top"><br>
						Back to Top
					</a>
					<div class="contact_details d-none d-lg-block">
					<?php
						echo "
							<a href=\"tel:{$office['phone']}\">{$office['phone']}</a><br>
							{$office['address']['address']}<br>
							<a href=\"mailto:{$office['email']}\">{$office['email']}</a>
						";
					?>
					</div>

				</div>
				<div class="order-3 d-none d-lg-block col-lg-2">
					<?php //content area ?> 
				</div>
				<div class="order-4 col-lg-2 nav-col">
     		    <?php wp_nav_menu( array('theme_location' => 'footer', 'items_wrap' => '<ul class="footer-nav">%3$s</ul>', 'container' => false, 'fallback_cb' => false) ); ?> 
				</div>
				<div class="order-6 order-lg-5 col-lg-1">
					
				</div>
				<div class="order-5 order-lg-6 col-lg-3 search-block">
					<form class="search-form d-none d-lg-block" method="post" action="<?php echo home_url( '/' ); ?>">
						<input type="search" name="s" id="search_bar" value="" placeholder="">
						<input type="submit" value="Search">
					</form>

					<?php include('module/social.php'); ?>
				</div>
			</div>
		</div>
	</footer>

	<!-- Javascript -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAAJQPmuLS8Unr0G9Hm44NU8G8es5Hi_hM"></script>
	<script src="<?php bloginfo('template_url'); ?>/assets/js/script.min.js?version=<?=$buster?>"></script>

	<!-- Wordpress Footer -->
	<?php wp_footer(); ?>

</body>
</html>
