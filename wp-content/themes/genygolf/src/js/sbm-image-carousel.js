	// Set up for SBM Carousel
	var image_set = 0;

	function imageCarouselSBM(item,wrapper) {
		this.num = item;
		this.name = wrapper;
		this.size = { w:parseInt($(this.name+' ul').width()), h:parseInt($(this.name+' ul').height()) };
		this.item = { w:this.size.w / sbm_carousel.columns, count:0 };
		this.row = { w:0 };
		this.dir = 0;
		this.pos = 0;
		this.posO = 0;
		this.children = [];
		this.slider = sbm_carousel.slider;
		this.is_mobile = false;
		this.init = function(is_mobile) {
			this.is_mobile = is_mobile;
			if (sbm_carousel.debug) console.log("> Initialise SBM Carousel ("+((this.is_mobile)?"Mobile":"Desktop")+")");
			var item_width = this.item.w;
			if (this.is_mobile) {
				this.item.w = sbm_carousel.mobile_img;
				item_width = this.item.w;
			}
			var children = [];
			var this_obj = this;
			var slider = this.slider;
			$(this.name+' li').each(function(index) {
				var new_item = null;
				if (slider) {
					new_item = new imageCarouselItem(this_obj, $(this), index, item_width);
					new_item.init(sbm_carousel.child[this_obj.num].is_mobile);
				} else {
					new_item = new sbmFadeCarouselItem(this_obj, $(this), index);
				}
				children.push(new_item);
			});
			this.children = children;
			if (sbm_carousel.debug) console.log("   - Children: "+this.children.length);

			// Slider Funcationality
			if (this.slider) {
				// this.row.w = this.size.w * this.item.count;

				if (this.children.length > sbm_carousel.columns) {
					if (!this.is_mobile && this.children.length > (sbm_carousel.columns+1)) {
						var last_item = this.children.length - 1;
						this.children[last_item].left = -this.item.w;
						this.children[last_item].target = -this.item.w;
						this.children[last_item].redraw();
					}
				} else {
					// Hide Controls
					if (!this.is_mobile) {
						$(this.name+' .action-left').css('display','none');
						$(this.name+' .action-right').css('display','none');
					}
				}
			}
		};
		this.update = function(step_x) {
			// Slider Funcationality
			if (this.slider) {
				this.children[this.pos].obj.addClass('focus');
				this.children[this.posO].obj.removeClass('focus');
				for (var i=0; i<this.children.length; i++) {
					var node = this.children[i];
					node.target += step_x;
					node.update();
				}
			} else {
				// Fader Functionality
				console.log($(this.children[this.pos]));
				this.children[this.pos].fadeIn();
				this.children[this.posO].fadeOut();
			}
		};
	}

	function sbmFadeCarouselItem(parent, obj, index) {
		this.parent = parent;
		this.obj = obj;
		this.index = index;
		this.timer = null;
		this.delay = 510;
		this.init = function() {
			clearTimeout(this.timer);
			$(this.obj).css({ 'z-index':'1' }).removeClass('fadeOut');
		};
		this.fadeOut = function() {
			$(this.obj).addClass('fadeOut');
			var obj = this.parent.children[this.index];
			this.timer = setTimeout(function() {
				obj.init();
			},this.delay);
		}
		this.fadeIn = function() {
			$(this.obj).css({'z-index':'4', 'display':'block'});
			var obj = this.parent.children[this.index];
			this.timer = setTimeout(function() {
				$(obj.obj).css({'z-index':'5'});
			},this.delay);
		}
	}

	function imageCarouselItem(parent, obj, index, width) {
		this.parent = parent;
		this.obj = obj;
		this.index = index;
		this.left = index * width;
		this.target = this.left;
		this.width = width;
		this.timer = null;
		this.speed = 10;
		this.step = 0;
		this.dir = 0;
		this.fraction = 0;
		this.is_mobile = false;
		this.offset = ($('.image-carousel-wrap').width() - width)/2;
		this.init = function(is_mobile) {
			this.is_mobile = is_mobile;
			if (this.is_mobile) {
				this.left = this.offset + this.width * this.index;
				if (this.index == 0) this.obj.addClass('focus');
			} else {
				this.left = this.width * this.index;
			}
			this.target = this.left;
			this.redraw();
		};
		this.redraw = function() {
			this.obj.css({ 'left':this.left+'px', 'width':this.width+'px' });
		};
		this.update = function() {
			clearTimeout(this.timer);
			this.step = ((this.target - this.left)/15) + this.fraction;
			this.fraction = this.step - Math.floor(this.step);

			if (this.target > this.left) {
				this.parent.dir = 1;
				this.step = (this.step > 1)?this.step:1;
				this.left += this.step;
				if (this.left >= this.target) {
					this.left = this.target;
				}
			} else if (this.target < this.left) {
				this.parent.dir = -1;
				this.step = (this.step < -1)?this.step:-1;
				this.left += this.step;
				if (this.left <= this.target) {
					this.left = this.target;
				}
			}
			this.redraw();
			// Check if out of bounds
			if (this.parent.dir == -1 && this.left <= (0 - this.width * 2)) {
				if (!this.is_mobile) {
					var newPos = this.parent.children.length + this.index - this.parent.pos;
					if (newPos > this.parent.children.length) {
						newPos -= this.parent.children.length;
					}
					this.left = this.width * newPos;
					this.target = this.left;
					this.redraw();
				}
			} else if (this.parent.dir == 1 && this.left >= this.parent.size.w + this.width) {
				if (!this.is_mobile) {
					var newPos = this.parent.children.length - this.index + this.parent.pos;
					if (newPos > this.parent.children.length) {
						newPos -= this.parent.children.length;
					}
					this.left = this.width * -newPos;
					this.target = this.left;
					this.redraw();
				}
			} else if (this.left != this.target) {
				var i = this.index;
				var p = this.parent.num;
				this.timer = setTimeout(function() {
					sbm_carousel.child[p].children[i].update();
				},this.speed);
			}
		}
		return this;
	}

	var autoTimer = null;
	$(function() {
		if ($('.sbm-image-carousel').length > 0) {
			$img_carousel = $('.sbm-image-carousel');
			if (!sbm_carousel.autoScroll) {
				$pos = 0;
				$img_carousel.each(function() {
					$container = $(this).attr('id');
					var $cont = $('#'+$container).find('ul');
					var l_arrow = $('<div>').addClass('action action-left action-left-'+$pos+' p4-caret-left').attr('data-control',$pos);
					$cont.append(l_arrow);
					var r_arrow = $('<div>').addClass('action action-right action-right-'+$pos+' p4-caret-right').attr('data-control',$pos);
					$cont.append(r_arrow);
					$pos++;
				});
			}
		}

		$pos = 0;
		if ($('.sbm-image-carousel').length > 0) {
			$img_carousel = $('.sbm-image-carousel');
			$img_carousel.each(function() {
				$container = $(this).attr('id');
				sbm_carousel.child[$pos] = new imageCarouselSBM($pos,'#'+$container);  // '.sbm-image-carousel'
				var is_mobile = ($(window).width() <= sbm_carousel.mobile);
				image_set = is_mobile;
				sbm_carousel.child[$pos].init(is_mobile);
				if (sbm_carousel.autoScroll) {
					autoTimer = setTimeout('autoScroll()',sbm_carousel.delay);
				} else {
					if (sbm_carousel.child[$pos].is_mobile || (sbm_carousel.child[$pos].children.length > sbm_carousel.columns)) {
						//
						// Right Control
						//
						$('.action-right-'+$pos).on('click',function() {
							$pos = $(this).data('control');
							if (sbm_carousel.debug) console.log("   - Click right arrow ("+$pos+")!");
							sbm_carousel.child[$pos].posO = sbm_carousel.child[$pos].pos++;

							// IF Number of cards is only 1 more than whats being shown
							// :: carousel.length == columns + 1
							if ((sbm_carousel.child[$pos].children.length == sbm_carousel.columns+1) && !sbm_carousel.child[$pos].is_mobile) {
								var temPos = sbm_carousel.child[$pos].posO + sbm_carousel.columns; 
								if (temPos >= sbm_carousel.child[$pos].children.length) {
									temPos -= sbm_carousel.child[$pos].children.length;
								}
								sbm_carousel.child[$pos].children[temPos].left = sbm_carousel.child[$pos].children[temPos].width * sbm_carousel.columns;
								sbm_carousel.child[$pos].children[temPos].target = sbm_carousel.child[$pos].children[temPos].left;
								sbm_carousel.child[$pos].children[temPos].redraw();
							}

							if (sbm_carousel.child[$pos].pos > sbm_carousel.child[$pos].children.length-1) {
								sbm_carousel.child[$pos].pos = 0;
								if (sbm_carousel.child[$pos].is_mobile && sbm_carousel.mobile_bounce) {
									sbm_carousel.child[$pos].pos = sbm_carousel.child[$pos].children.length-1;
								} else {
									sbm_carousel.child[$pos].update(-sbm_carousel.child[$pos].item.w);
								}
							} else {
								sbm_carousel.child[$pos].update(-sbm_carousel.child[$pos].item.w);
							}
						});
						//
						// Right Control
						//
						$('.action-left-'+$pos).on('click',function() {
							if (sbm_carousel.debug) console.log("   - Click left arrow");
							$pos = $(this).data('control');
							sbm_carousel.child[$pos].posO = sbm_carousel.child[$pos].pos--;

							// IF Number of cards is only 1 more than whats being shown
							// :: carousel.length == columns + 1
							if ((sbm_carousel.child[$pos].children.length == sbm_carousel.columns+1) && !sbm_carousel.child[$pos].is_mobile) {
								var temPos = sbm_carousel.child[$pos].pos; 
								if (temPos < 0) {
								 	temPos = sbm_carousel.child[$pos].children.length - 1;
								}
								sbm_carousel.child[$pos].children[temPos].left = -sbm_carousel.child[$pos].children[temPos].width;
								sbm_carousel.child[$pos].children[temPos].target = sbm_carousel.child[$pos].children[temPos].left;
								sbm_carousel.child[$pos].children[temPos].redraw();
							}

							if (sbm_carousel.child[$pos].pos < 0) {
								sbm_carousel.child[$pos].pos = sbm_carousel.child[$pos].children.length - 1;
								if (sbm_carousel.child[$pos].is_mobile && sbm_carousel.mobile_bounce) {
									sbm_carousel.child[$pos].pos = 0;
								} else {
									sbm_carousel.child[$pos].update(sbm_carousel.child[$pos].item.w);
								}
							} else {
								sbm_carousel.child[$pos].update(sbm_carousel.child[$pos].item.w);
							}
						});
					}
				}
				// Add hover state
				for (var i=0 ;i<sbm_carousel.child[$pos].children.length; i++) {
					var li = $(sbm_carousel.child[$pos].children[i].obj);
					var a = $(li).find('a');
					// a.hover(function(event) { hoverEvent(event,'in'); }, function() { hoverEvent(event,'out'); });
				}
				$pos++;
			});
		}
	});

	function autoScroll() {
		clearTimeout(autoTimer);
		sbm_carousel.child[$pos].posO = sbm_carousel.child[$pos].pos;
		if (++sbm_carousel.child[$pos].pos > sbm_carousel.child[$pos].children.length-1) {
			sbm_carousel.child[$pos].pos = 0;
		}
		sbm_carousel.child[$pos].update(-sbm_carousel.child[$pos].item.w);
		autoTimer = setTimeout('autoScroll()',sbm_carousel.delay);				
	}
