/* STICKY NAV
*/
var stickyPos;
var stickyTarget;
var stickyTargetOffset;

console.log("Mobile : "+isMobile.any());

if ($('.cta-buttons').length > 0) {
  stickyPos = $('header').height();
  stickyTarget = $('.cta-buttons');
  stickyTargetOffset = $(window).height() - stickyPos;
}

function ctaButtons(){
  if($(window).scrollTop() + stickyPos > stickyTargetOffset){
    stickyTarget.addClass('sticky');
  } else{
    stickyTarget.removeClass('sticky');
  }
}

$(function() {
  if ($('.cta-buttons').length > 0 && !isMobile.any()) {
    ctaButtons();
  }
});

$(window).on('scroll', function(){
  if ($('.cta-buttons').length > 0 && !isMobile.any()) {
    ctaButtons();
  }
});
