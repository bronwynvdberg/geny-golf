/* CAROUSEL
*/
var cta_buttons = 0;

$(function(){
	if ($('#carousel_home').length > 0) {
		cta_buttons = $('.cta-buttons').height() - 3;  // Cater for borders
		resetCarousel();
	}
});

$(window).resize(function() {
	if ($('#carousel_home').length > 0) {
		resetCarousel();
	}
});

// Set Carousel height based on page width
function resetCarousel() {
	var page   = { 'w':$(window).width(), 'h':$(window).height() };
	var height = page.h - cta_buttons;

	// Not using global settings, need full screen!
	//
	// var height = $('.carousel-inner').data('height');
	// if (page.width < 768) {
	// 	new_height *= 0.75;
	// }
	// else if (page.width < 993) {
	// 	new_height *= 0.75;
	// }

	$('.carousel-inner').css('height',height+'px');
	$('.carousel-item .img').each(function() {
		$(this).css('height',height+'px');
	});
}