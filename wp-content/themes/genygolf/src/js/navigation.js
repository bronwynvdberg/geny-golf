/* NAVIGATION
*/
$(document).ready(function(){

	// Hamburger Menu
	var closing_menu = false;
	$('#navbar-toggle').click(function(){
		if (!closing_menu) {
			$(this).toggleClass('open');
			if ($(this).hasClass('open')) {
				$('.cover').css('right','0');
				$('body').addClass('menu-open');
				$('.cover').animate({opacity:'1'},500, function() {});
			} else {
				$('body').removeClass('menu-open');
				$('.cover').animate({opacity:'0'},500, function() {
					$(this).css('right','9999em');
				});
			}
		}
	});

	// Mobile Menu Cover
	$('.cover').click(function() {
		if ($('#navbar-toggle').hasClass('open')) {
			closing_menu = true;
			$('#navbar-toggle').removeClass('open');
			$('.cover').animate({opacity:'0'},500, function() {
				$(this).css('right','9999em');
				$('body').removeClass('menu-open');
				closing_menu = false;
			});
		}
	});

});