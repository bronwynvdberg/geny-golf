<?php get_header(); ?>		

		<!-- BANNER -->
		
		<div class="banner">
		<div class="container">
		<div class="bannerwrap">
					
			<img src="<?php bloginfo('template_url'); ?>/assets/images/banners/banner-sm.jpg" class="img-responsive">

		</div><!--/.bannerwrap -->
		</div><!--/.container -->
		</div><!--/.banner -->

		
		<!-- CONTENT -->
		
		<div class="content">
		<div class="container">
		<div class="contentwrap">
		<div class="row">
				
			<div class="col-sm-8">
			<div class="main">

				<div class="breadcrumbs"><?php if(function_exists('bcn_display')) { bcn_display(); }?></div>
			 	
				<h1>Testimonials</h1>	
				 	
				 	<?php if ( have_posts() ) : ?>
				 	
				 	<ul class="list-unstyled list-testimonial">
				 	
					<!-- Loop -->
					<?php while ( have_posts() ) : the_post();

						$testimoniallong=get_post_meta($post->ID, 'testimonial_long', true);
						$testimonialperson=get_post_meta($post->ID, 'testimonial_person', true);
						$testimoniallocation=get_post_meta($post->ID, 'testimonial_location', true);

					?>
						<li>
						<div class="well well-testimonial" itemscope itemtype="http://schema.org/Review">
							<span class="review" itemprop="reviewBody"><?php the_field('testimonial_long'); ?></span>
							<?php if ( $testimonialperson ) : ?><span class="author" itemprop="author" itemscope itemtype="http://schema.org/Person"><span itemprop="name"><?php the_field('testimonial_person'); ?></span><?php endif; ?>
							<?php if ( $testimoniallocation ) : ?>, <?php the_field('testimonial_location'); ?><?php endif; ?>
							<?php if ( $testimonialperson ) : ?></span><?php endif; ?>
						</div>
						</li>
						
					<?php endwhile; ?>
					
				 	</ul>
				 	
				 	<div class="row">
				 		<div class="navprevious col-xs-6"><?php next_posts_link( '&laquo; Older ' ); ?></div>
				 		<div class="navnext col-xs-6"><?php previous_posts_link( 'Newer &raquo;' ); ?></div>
				 	</div>
			        
			        <?php else : ?>
			        
			            <p>No posts available</p>
			            
			        <?php endif; ?>
					<!-- END Loop -->

			</div> 
			</div><!--/.main -->
			
			<div class="col-sm-4">
			<div class="aside">
				
				<?php get_sidebar(); ?>

			</div>			    
			</div><!--/.aside -->
				
		</div><!--/.row -->
		</div><!--/.contentwrap -->
		</div><!--/.container -->
		</div><!--/.content -->
		
<?php get_footer(); ?>