<form role="search" method="get" class="search-form" action="<?php echo site_url(); ?>">
<div class="input-group">
<input type="search" class="form-control" style="height:40px;"
    placeholder="<?php echo esc_attr_x( 'Search the site…', 'placeholder' ) ?>"
    value="<?php echo get_search_query() ?>" name="s"
    title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
    <span class="input-group-btn">
      <input type="submit" class="btn btn-default" style="height:40px;"
          value="<?php echo esc_attr_x( 'Search', 'submit button' ) ?>" />
    </span>
  </div><!-- /input-group -->
</form>
