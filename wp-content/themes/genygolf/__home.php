<?php 
	get_header(); 
	
	include("module/banner.php");
?>
		
	<div class="content">
	<div class="container">
	<div class="contentwrap">
	<div class="row">
			
		<div class="col-sm-8">
		<div class="main">
		 	
			<div class="breadcrumbs"><?php if(function_exists('bcn_display')) { bcn_display(); }?></div>

			<h1>
				<?php if ( is_category() ) :
			    	printf( __( '%s' ), single_cat_title( '', false ) );
			    else :
			    	_e( 'Archives' );
			    endif; ?>
			</h1>	
			 	
			 	<?php if ( have_posts() ) : ?>
			 	
			 	<ol class="list-unstyled list-archives">
			 	
				<!-- Loop -->
				<?php while ( have_posts() ) : the_post(); ?>
					
					<li>
					
						<h2><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h2>
						
						<div class="meta">Posted: <?php the_time('F j Y') ?></div>

						<?php the_excerpt(); ?>
				
					</li>
					
				<?php endwhile; ?>
				
			 	</ol>
			 	
			 	<div class="row">
			 		<div class="navprevious col-xs-6"><?php next_posts_link( '&laquo; Older ' ); ?></div>
			 		<div class="navnext col-xs-6"><?php previous_posts_link( 'Newer &raquo;' ); ?></div>
			 	</div>
		        
		        <?php else : ?>
		        
		            <p>No posts available</p>
		            
		        <?php endif; ?>
				<!-- END Loop -->

		</div> 
		</div><!--/.main -->
		
		<div class="col-sm-4">
		<div class="aside">
			
			<?php get_sidebar(); ?>

		</div>			    
		</div><!--/.aside -->
			
	</div><!--/.row -->
	</div><!--/.contentwrap -->
	</div><!--/.container -->
	</div><!--/.content -->
	
<?php get_footer(); ?>