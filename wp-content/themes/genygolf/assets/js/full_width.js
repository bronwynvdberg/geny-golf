/* FULL WIDTH
*/
	// Old school way : not used.
	var winW = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;

	function place_image(target, src, alt, width, height) {
		var $img = $("<img>").attr('src',src).attr('alt',alt).attr('width',width).css('min-height',height+'px');
		$img.addClass((width < $(window).width())?"img-full img-extend":"img-full");
		$('#'+target).append($img);
	}