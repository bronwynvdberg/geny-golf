<?php

  // session_start();

  // Header
	get_header();
	$content = Content::FromPost('single');
	
	// Banner
	include('module/banner.php');
	// CONTENT
?>
		<div class="content">
			<div class="container">
				<div class="row">
					<?php
						// Breadcrumbs
						// $page_title = $content->title;
						include('module/breadcrumbs.php');
					?>
					<div class="col-12 post-block">
						<div class="row">
							<div class="col-lg-6">
								<?php
									 echo $content->copy;
								?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php get_footer(); ?>
