# Changelog

## [1.1.17](https://github.com/caseproof/memberpress-developer-tools/releases/tag/1.1.17) - 2018-09-05

A minor release that deals with a change for password reset.

### Changed

- Use MemberPress functions to send password resets rather than `wp_`
