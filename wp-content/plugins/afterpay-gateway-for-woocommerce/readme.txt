=== Afterpay Gateway for WooCommerce ===
Contributors: afterpayit
Tags: woocommerce, afterpay
Requires at least: 3.5.0
Tested up to: 4.9.8
Stable tag: 2.0.3
License: GNU Public License
License URI: https://www.gnu.org/licenses/

Provide Afterpay as a payment option for WooCommerce orders.

== Description ==

Give your customers the option to buy now and pay later with Afterpay. The "Afterpay Gateway for WooCommerce" plugin provides the option to choose Afterpay as the payment method at the checkout. It also provides the functionality to display the Afterpay logo and instalment calculations below product prices on category pages, individual product pages, and on the cart page. For each payment that is approved by Afterpay, an order will be created inside the WooCommerce system like any other order. Automatic refunds are also supported.

== Installation ==

This section outlines the steps to install the Afterpay plugin.

> Please note: If you are upgrading to a newer version of the Afterpay plugin, it is considered best practice to perform a backup of your website - including the WordPress database - before commencing the installation steps. Afterpay recommends all system and plugin updates to be tested in a staging environment prior to deployment to production.

1. Login to your WordPress admin.
1. Navigate to "Plugins > Add New".
1. Type "Afterpay" into the Keyword search box and press the Enter key.
1. Find the "Afterpay Gateway for WooCommerce" plugin. Note: the plugin is made by "Afterpay".
1. Click the "Install Now" button.
1. Click the "Activate" button.
1. Navigate to "WooCommerce > Settings".
1. Click the "Checkout" tab.
1. Click the "Afterpay" sub-tab.
1. Enter the Merchant ID and Secret Key that were provided by Afterpay for Production use.
1. Save changes.

== Frequently Asked Questions ==

= What do I do if I need help? =

Please visit the official [Afterpay Help Centre](https://help.afterpay.com/hc) online. Most common questions are answered in the FAQ. There is also the option to create a support ticket if necessary.

== Changelog ==

= 2.0.3 =
*Release Date: Tuesday, 11 September 2018*

* Improved support for custom meta fields for WooCommerce orders.
* Improved compatibility with third-party currency switcher plugins.
* Improved handling of WooCommerce order line items.

= 2.0.2 =
*Release Date: Wednesday, 29 August 2018*

* Improved support for Variable Products.
* Improved handling of network challenges in scheduled background tasks.

= 2.0.1 =
*Release Date: Thursday, 19 July 2018*

* Added support for Afterpay assets to display on product and cart pages where prices are outside merchant payment limits.
* Added support for multi-market use in Australia, New Zealand and United States.
* Improved logging of network challenges.
* Improved Afterpay JavaScript initialisation to cater for transactions from Australia, New Zealand and the United States.
* Improved handling of Afterpay pop-up assets - deprecated the use of fancyBox.
* Improved reliability of payment capture and refunds - implemented a retry mechanism in the unlikely event of network challenges.
* Updated plugin configuration defaults for each regional market (AU/NZ/US).
* Updated assets for Afterpay United States.

= 2.0.0 =
*Release Date: Friday, 13 July 2018*

* Added support for merchants in New Zealand and the United States.
* Added support for the calculation of instalment amounts at the product level for variably priced products.
* Added support for orders that do not require shipping addresses.
* Added support for optionally including Afterpay elements on the cart page.
* Added a shortcode for rendering the standard Afterpay logo, with support for high pixel-density screens and a choice of 3 colour variants.
* Improved ease of installation and configuration.
* Improved presentation of checkout elements for various screen sizes.
* Improved customisability for developers.
* Changed order button to read "Proceed to Afterpay" when configured to use the "v1 (recommended)" API Version.
* Changed the payment declined messages to include the phone number for the Afterpay Customer Service Team.
* Changed the default HTML for category pages and individual product pages to take advantage of the latest features.
* Changed the plugin name from "WooCommerce Afterpay Gateway" to "Afterpay Gateway for WooCommerce".
* Removed deprecated CSS.

= 1.3.1 =
*Release Date: Monday, 10 April 2017*

* Improved compatibility with WooCommerce 3 - resolution of the "invalid product" checkout challenge.
