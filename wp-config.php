<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

 // define('WP_HOME','http://localhost/framework/');
 // define('WP_SITEURL','http://localhost/framework/');

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'futuregolf');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'ACmi=_L](9%+|8z|~G,CWPPVfV6Nrz @@fpbetQ(r&FM8GT=A5J9^9:nD!9us|V;');
define('SECURE_AUTH_KEY',  'M:KM{^D%X61SttJj}-Ln@GfiR5;@sv_C=I+XEr]:vCw<=/yc!LtKAJyB|->V!||F');
define('LOGGED_IN_KEY',    'ZgkJk*I,$R,wrL9(y3pr|l#:|e y,{Mq$--V{Lxv/8Qy#Kki t=`yie|uBQ%Q{(D');
define('NONCE_KEY',        'l7B.8.hPg^xYrAcQGx*KuCf %Qh*`b?=7&SO-,a[vt_7DeTGx<msh>H+-R9Cv/vr');
define('AUTH_SALT',        '>YL5l$3t.@WkQHGkyoG#?DOH])F.yIY(+3QEZ=AY)oZpry :Pv~G=Nx}|]+q!8=)');
define('SECURE_AUTH_SALT', 'sljPgmS+)={nj_[/?:y35L|0NP-8@*C+_Jf$=dNKaApnm<hoOO&_e1[Kemy_HrHR');
define('LOGGED_IN_SALT',   'f.;p22+^fWB=q/85p;J=b$u$ hR?t`NwpMBr[+&-4,C90[|ZE)45Z,Xhq-A-DvNJ');
define('NONCE_SALT',       '/3zr4I4[zUD[t`2Wc1;Yd,rSm=[1=O!el?9&pnY,v!u97KCIq$i=Mk-HmB:zQGc<');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'sbm_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */

/*
	Limit post revisions
	- keeps the database clean site running smooth.
*/
define( 'WP_POST_REVISIONS', 3 );
/*
	Cache
	- includes cache core script
*/
define( 'WP_CACHE', true );

define('WP_DEBUG', false);
define('WP_AUTO_UPDATE_CORE', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
